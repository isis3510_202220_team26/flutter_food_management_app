import 'package:flutter/cupertino.dart';

class HouseItem with ChangeNotifier {
  final String name;
  final String quantity;
  final String measurement;
  final DateTime expireDate;

  HouseItem({
    required this.name,
    required this.quantity,
    required this.measurement,
    required this.expireDate,
  });
}
