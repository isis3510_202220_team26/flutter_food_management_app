import './houseitem.dart';

class Pantry {
  final String name;
  final List<HouseItem> houseItems;

  Pantry(this.name, this.houseItems);
}
