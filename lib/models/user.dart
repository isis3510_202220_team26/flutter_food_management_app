import 'package:flutter/cupertino.dart';
import 'package:flutter_food_management_app/models/houseitem.dart';

class Usuario with ChangeNotifier {
  final String email;
  final String firstName;
  final String lastName;
  int? age;
  int? dailyCalories;
  final String categories;
  List<HouseItem> hs;
  //final String userName;
  final DateTime lastPantry;

  Usuario({
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.categories,
    required this.age,
    required this.dailyCalories,
    required this.hs,
    //required this.userName,
    required this.lastPantry
  });
}
