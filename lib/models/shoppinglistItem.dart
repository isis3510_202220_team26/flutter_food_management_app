import 'package:flutter/cupertino.dart';

class ShoppingListItem with ChangeNotifier {
  final String name;
  final String quantity;
  final String measurement;
  final String description;
  final String img;
  final DateTime expireDate;

  ShoppingListItem(
      {required this.name,
      required this.quantity,
      required this.measurement,
      required this.expireDate,
      required this.description,
      required this.img});
}
