import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

import '../daos/recipe_dao.dart';
import '../daos/recipe_dao_firebase.dart';

class Recipe with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  bool downloaded;
  String imageURL;
  int numberOfLikes;
  bool liked;

  Recipe({
    required this.id,
    required this.title,
    required this.description,
    this.imageURL = 'NONE',
    this.numberOfLikes = 0,
    this.liked = false,
    this.downloaded = false,
  });

  factory Recipe.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();
    return Recipe(
      description: data?['description'],
      id: snapshot.id,
      imageURL: data?['imageURL'],
      title: data?['title'],
      numberOfLikes: data?['numberOfLikes'],
    );
  }

  factory Recipe.fromSQLite(Map<String, dynamic> data) {
    return Recipe(
      id: data['id'],
      title: data['title'],
      description: data['description'],
      downloaded: true,
    );
  }

  Map<String, dynamic> toSQLite() {
    return {
      'description': description,
      'id': id,
      'title': title,
    };
  }

  Map<String, dynamic> toFirestore() {
    return {
      "description": description,
      "id": id,
      "imageURL": imageURL,
      "title": title,
      "numberOfLikes": numberOfLikes,
    };
  }

  static List<Recipe> dataListFromSnapshot(QuerySnapshot querySnapshot) {
    return querySnapshot.docs.map((snapshot) {
      final Map<String, dynamic> dataMap =
          snapshot.data() as Map<String, dynamic>;

      return Recipe(
        title: dataMap['title'],
        imageURL: dataMap['imageURL'],
        id: dataMap['title'],
        description: dataMap['description'],
        numberOfLikes: dataMap['numberOfLikes'],
      );
    }).toList();
  }

  Future<void> toggleFavoriteStatus(String authID) async {
    RecipeDAO recipeDao = RecipeDAOFirebaseImpl();

    final oldStatus = liked;
    liked = !liked;

    if (liked == true) {
      numberOfLikes += 1;
    } else {
      numberOfLikes -= 1;
    }

    await recipeDao.updateFavoriteStatusDB(authID, id, numberOfLikes, liked);

    notifyListeners();
  }

  void changeDownloaded() {
    downloaded = true;
    notifyListeners();
  }
}
