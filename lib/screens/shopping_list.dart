import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_food_management_app/constants.dart';
import 'package:flutter_food_management_app/controller/pantry_controller.dart';
import 'package:flutter_food_management_app/controller/shoppinglist_controller.dart';
import 'package:flutter_food_management_app/models/shoppinglistItem.dart';
import 'package:intl/intl.dart';

import '../models/houseitem.dart';
import '../test_variables.dart';

class ShoppingList extends StatelessWidget {
  //const ShoppingList({super.key});
  static const routeName = 'ShoppingListScreen';
  final ShoppingListController _controller = ShoppingListController();
  final PantryController _pController = PantryController();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        _controller.getShoppingListItems(),
        _controller.checkConnectivity(),
      ]),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text('${snapshot.error} occurred',
                  style: TextStyle(fontSize: 18)),
            );
          } else if (snapshot.hasData) {
            List<ShoppingListItem>? data = snapshot.data![0];
            data?.add(ShoppingListItem(
                name: 'name',
                img: '',
                quantity: 'quan',
                description: 'd',
                measurement: 'm',
                expireDate: DateTime.parse('2022-02-27')));

            print(data![0].img);
            return Scaffold(
              backgroundColor: kRufouse,
              appBar: AppBar(
                title: const Center(
                  child: Text(
                    "Shopping List",
                    style:
                        TextStyle(color: kRufouse, fontWeight: FontWeight.bold),
                  ),
                ),
                backgroundColor: kYellowColor,
              ),
              body: SingleChildScrollView(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: data.map((sh) {
                        if (sh.name == 'name') {
                          if (snapshot.data![1]) {
                            return const Text('');
                          } else {
                            return const Text(
                              'No se esta mostrando datos mas recientes. Revisar conectividad',
                              style:
                                  TextStyle(fontSize: 15, color: kYellowColor),
                              textAlign: TextAlign.center,
                            );
                          }
                        } else {
                          return buildCard(sh.name, sh.description, sh.quantity,
                              sh.measurement, sh.img, _pController);
                        }
                      }).toList(),
                      //<Widget>[],
                    ),
                  ),
                ),
              ),
            );
          }
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}

Card buildCard(n, d, q, m, img, PantryController p) {
  return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: CachedNetworkImage(
              imageUrl: img,
            ),
            title: Text(n),
            subtitle: Text(d),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: <Widget>[
                const Spacer(),
                Column(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 20,
                      backgroundColor: kRufouse, //<-- SEE HERE
                      child: IconButton(
                        icon: const Icon(
                          Icons.add,
                          color: kYellowColor,
                        ),
                        onPressed: () {
                          var temp = ShoppingListItem(
                              name: n,
                              description: d,
                              quantity: q,
                              measurement: m,
                              img: img,
                              expireDate: DateTime.parse('2022-02-27'));
                          p.addHI2User(temp);
                        },
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      q + ' ' + m,
                      style: TextStyle(
                        fontWeight: FontWeight.w100,
                        fontSize: 10,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          )
        ],
      ));
}
