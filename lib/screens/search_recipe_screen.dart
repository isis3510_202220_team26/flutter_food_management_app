import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firestore_search/firestore_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_food_management_app/screens/recipe_details.dart';
import 'package:provider/provider.dart';

import '../models/recipe.dart';
import '../providers/internet_provider.dart';

class SearchRecipeScreen extends StatefulWidget {
  static const routeName = '/RecipeSearch';
  static CacheManager cacheManager = CacheManager(
    Config(
      'recipeSearchCacheKey',
      stalePeriod: const Duration(hours: 2),
    ),
  );

  const SearchRecipeScreen({Key? key}) : super(key: key);

  @override
  State<SearchRecipeScreen> createState() => _SearchRecipeScreenState();
}

class _SearchRecipeScreenState extends State<SearchRecipeScreen> {
  var _isInit = true;
  var _isLoading = false;
  var _isConnectedToInternet = false;

  @override
  void didChangeDependencies() async {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      InternetProvider internetProvider =
          Provider.of<InternetProvider>(context, listen: false);
      Future.delayed(const Duration(milliseconds: 300)).then(
        (value) => internetProvider.testConnection().then(
          (value) {
            if (internetProvider.connected) {
              setState(() {
                _isConnectedToInternet = true;
              });
            } else {
              setState(() {
                _isConnectedToInternet = false;
              });
            }
            setState(() {
              _isLoading = false;
            });
            _isInit = false;
          },
        ),
      );
    }
    super.didChangeDependencies();
  }

  Future<bool> _stateTestConnection() async {
    InternetProvider internetProvider =
        Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();
    log('connected in view: ${internetProvider.connected}');
    if (internetProvider.connected) {
      setState(() {
        _isConnectedToInternet = true;
      });
    } else {
      setState(() {
        _isConnectedToInternet = false;
      });
    }
    return internetProvider.connected;
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Scaffold(
            body: Center(child: CircularProgressIndicator()),
          )
        : _isConnectedToInternet
            ? FirestoreSearchScaffold(
                dataListFromSnapshot: Recipe.dataListFromSnapshot,
                backButtonColor: Theme.of(context).colorScheme.secondary,
                appBarTitle: 'Search Foodia',
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    final List<Recipe>? dataList = snapshot.data;
                    if (dataList!.isEmpty) {
                      return const Center(
                        child: Text('No Results Returned'),
                      );
                    }
                    return SafeArea(
                      child: ListView.builder(
                          itemCount: dataList.length,
                          itemBuilder: (context, index) {
                            final Recipe data = dataList[index];

                            return ListTile(
                              onTap: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }

                                Navigator.of(context).pushNamed(
                                  RecipeDetails.routeName,
                                  arguments: {
                                    'id': data.id,
                                    'downloaded': data.downloaded
                                  },
                                );
                              },
                              leading: CircleAvatar(
                                radius: 50,
                                child: CachedNetworkImage(
                                  imageUrl: data.imageURL,
                                  width: double.infinity,
                                  height: 200,
                                  cacheManager: SearchRecipeScreen.cacheManager,
                                  fit: BoxFit.fitWidth,
                                  progressIndicatorBuilder:
                                      (context, url, downloadProgress) =>
                                          Container(
                                    color: Theme.of(context).backgroundColor,
                                    child: Center(
                                      child: CircularProgressIndicator(
                                          value: downloadProgress.progress),
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Image.asset(
                                          'assets/images/recipeAssetImage.png'),
                                ),
                              ),
                              title: Text(data.title),
                              subtitle: Text(
                                '${data.numberOfLikes} Likes',
                              ),
                            );
                          }),
                    );
                  }

                  if (snapshot.connectionState == ConnectionState.done) {
                    if (!snapshot.hasData) {
                      return const Center(
                        child: Text('No Results Returned'),
                      );
                    }
                  }
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                },
                firestoreCollectionName: 'recipes',
                searchBy: 'title',
                appBarBackgroundColor: Theme.of(context).colorScheme.secondary,
                scaffoldBody: const Center(),
              )
            : Scaffold(
                appBar: AppBar(
                  leading: IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: (() => Navigator.of(context).pop()),
                  ),
                ),
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text('No Internet Connection'),
                    TextButton(
                      child: const Text('REFRESH'),
                      onPressed: () async {
                        setState(() {
                          _isLoading = true;
                        });
                        await _stateTestConnection();
                        setState(() {
                          _isLoading = true;
                        });
                      },
                    ),
                  ],
                ),
              );
  }
}
