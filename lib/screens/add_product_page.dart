import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/controller/houseItem_controller.dart';
import 'package:flutter_food_management_app/controller/pantry_controller.dart';
import 'package:flutter_food_management_app/controller/user_controller.dart';
import 'package:flutter_food_management_app/models/houseitem.dart' as hi;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/screens/pantry.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_food_management_app/models/user.dart';

import '../models/houseitem.dart';
import '../providers/internet_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_food_management_app/constants.dart';

class AddProductPage extends StatefulWidget {


  @override
  State<AddProductPage> createState() => _AddProductPageState();
}

class _AddProductPageState extends State<AddProductPage> {
  //Controllers for firebase
  final _nameController = TextEditingController();
  final _quantityController = TextEditingController();
  final _measurementController = TextEditingController();

  String _dateTime= DateFormat.yMd().format(DateTime.now());

  var _isConnectedToInternet = false;

  void _showDatePicker() {
    showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime.now(),
      lastDate: DateTime(2025),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: Colors.amberAccent, //
              onPrimary: Colors.redAccent, //
              onSurface: Colors.blueAccent, //
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                foregroundColor: Colors.red, // button text color
              ),
            ),
          ),
          child: child!,
        );
      },
    ).then((value) {
      setState(() {
        _dateTime=DateFormat.yMd().format(value!);
      });
    });
  }

  Future addProduct() async {

    InternetProvider internetProvider =
    Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();
    //log('connected in view: ${internetProvider.connected}');

    final houseItem = hi.HouseItem(
        name: _nameController.text.trim(),
        quantity: _quantityController.text.trim(),
        measurement: _measurementController.text.trim(),
        expireDate: DateTime.now()
    );
    bool isInt=false;
    try{
      int.parse(houseItem.quantity);
      isInt=true;
    } catch(e){
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              content: Text("The quantity must be a number. Try again."),
            );
          }
      );
    }
    if(isInt){
      final db = FirebaseFirestore.instance;
      String newQuantity=houseItem.quantity;

      PantryController pController= PantryController();
      List<HouseItem> items=await pController.getHouseItems();
      for (var actual in items) {
        if(actual.name==_nameController.text.trim()){
          int bQuantity= int.parse(actual.quantity);
          int nQuantity= int.parse(houseItem.quantity);
          newQuantity=(bQuantity+nQuantity).toString();
        }
      }


      Usuario u=await UserController().getUserByEmail();
      db.collection('${'Users/' + u.email}/pantryHouse').doc(houseItem.name).set({
        "name": houseItem.name,
        "quantity": newQuantity,
        "measurement": houseItem.measurement,
        "expireDate": houseItem.expireDate,
      });


      //UDPATE USER ATRIBUTE OF LAST PANTRY


     db.collection("Users").doc(u.email).set({
        "email": u.email,
        "first name": u.firstName,
        "last name": u.lastName,
        "age": u.age,
        "dailyCalories": u.dailyCalories,
        "categories": u.categories,
        "last pantry": DateTime.now()
      });

      Navigator.push(context,
        MaterialPageRoute(builder: (context){
          return PantryScreen();
        },
        ),
      );//

      if (internetProvider.connected) {
        setState(() {
          _isConnectedToInternet = true;
        });

        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                content: Text("The product was added to your pantry"),
              );
            }
        );
      }
      else{
        setState(() {
          _isConnectedToInternet = false;
        });
        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                content: Text("There is no internet connection. Your product is pending for addition to the database. As soon as your internet connection is restored the process will resume. Do not try to add it again."),
              );
            }
        );
      }
    }

  }

  @override
  void dispose() {
    _nameController.dispose();
    _quantityController.dispose();
    _measurementController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kYellowColor,
        title: Center(
          child: Row(
            children: <Widget>[

              Text(
                "Add product",
                style: TextStyle(fontSize: 15, color: kRufouse),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
      backgroundColor: Color(0xff590004),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(height: 30),
                //Greetings
                Center(
                  child: Text(
                    "Add a product to your pantry",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32,
                      color: Color(0xffF3F3F3),

                    ),
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  "Enter information",
                  style: TextStyle(
                    fontSize: 20,
                    color: Color(0xffF3F3F3),
                  ),
                ),
                SizedBox(height: 15),

                //Email
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 15,
                        controller: _nameController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Name",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 8,
                        controller: _quantityController,
                        obscureText: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Quantity",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Confirm Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 10,
                        controller: _measurementController,
                        obscureText: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Measurement (ex.:units,kgs)",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //date
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: _showDatePicker,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8)),
                      child: Center(
                        child: Text(
                          "Choose expire date",
                          style: TextStyle(
                            color: Color(0xffA50104),
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                SizedBox(height: 10),

                //Button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: addProduct,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Colors.yellow,
                          borderRadius: BorderRadius.circular(12)),
                      child: Center(
                        child: Text(
                          "Add product",
                          style: TextStyle(
                            color: Color(0xffA50104),
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 25),


              ],
            ),
          ),
        ),
      ),
    );
  }
}

