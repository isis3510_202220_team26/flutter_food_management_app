import 'dart:developer' as dev;
import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_food_management_app/helpers/db_type.dart';
import 'package:flutter_food_management_app/models/pantry.dart';
import 'package:flutter_food_management_app/models/recipe.dart';
import 'package:flutter_food_management_app/providers/internet_provider.dart';
import 'package:flutter_food_management_app/providers/recipes_list_provider.dart';
import 'package:flutter_food_management_app/screens/pantry.dart';
import 'package:flutter_food_management_app/screens/search_recipe_screen.dart';
import 'package:flutter_food_management_app/test_variables.dart';
import 'package:flutter_food_management_app/widgets/page_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter_food_management_app/screens/auth_login_register/auth_page.dart';

import '../constants.dart';
import '../models/recipe.dart';
import '../widgets/category_menu.dart';
import '../widgets/recipe_list_item.dart';
import 'add_recipe.dart';
import 'user_profile.dart';

class RecipeOverview extends StatefulWidget {
  static const routeName = 'RecipeOverview';

  const RecipeOverview({super.key});

  @override
  State<RecipeOverview> createState() => _RecipeOverviewState();
}

class _RecipeOverviewState extends State<RecipeOverview> {
  var _isInit = true;
  var _isLoading = false;
  var _top5Loading = false;
  var _allLoading = false;
  var _isConnectedToInternet = false;
  var _switchOnlineState = true;
  int _active = 0;

  void logout() async {
    await FirebaseAuth.instance.signOut();
  }

  Future<bool> _stateTestConnection() async {
    InternetProvider internetProvider =
        Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();
    dev.log('connected in view: ${internetProvider.connected}');
    if (internetProvider.connected) {
      setState(() {
        _isConnectedToInternet = true;
      });
    } else {
      setState(() {
        _isConnectedToInternet = false;
      });
    }
    return internetProvider.connected;
  }

  @override
  void didChangeDependencies() async {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      InternetProvider internetProvider =
          Provider.of<InternetProvider>(context, listen: false);
      Future.delayed(const Duration(milliseconds: 300)).then(
        (value) => internetProvider.testConnection().then(
          (value) async {
            dev.log('connected in view: ${internetProvider.connected}');
            if (internetProvider.connected) {
              setState(() {
                _isConnectedToInternet = true;
              });
              await Provider.of<RecipesListProvider>(context, listen: false)
                  .getRecipes(
                DBType.firebase,
              );
              await Provider.of<RecipesListProvider>(context, listen: false)
                  .reload5Recipes(
                DBType,
              );
            } else {
              setState(() {
                _isConnectedToInternet = false;
              });
            }
            setState(() {
              _isLoading = false;
            });
            _isInit = false;
          },
        ),
      );
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final RecipesListProvider recipesProvider =
        Provider.of<RecipesListProvider>(context);
    final List<Recipe> recipes = recipesProvider.items;
    final List<Recipe> downloadedRecipes = recipesProvider.downloadedRecipes;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: _isLoading
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Loading Recipes...'),
                  ],
                ),
              )
            : Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 21.0),
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: <Widget>[
                            IconButton(
                              onPressed: logout,
                              icon: const Icon(
                                Icons.logout,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 5),
                                decoration: BoxDecoration(
                                  boxShadow: const [
                                    BoxShadow(color: Colors.black12)
                                  ],
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                alignment: Alignment.center,
                                child: TextButton(
                                  child: const Text(
                                    'Search Recipes',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(
                                      SearchRecipeScreen.routeName,
                                    );
                                  },
                                ),
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UserProfile()),
                                );
                              },
                              padding: const EdgeInsets.all(15.0),
                              icon: const Icon(FontAwesomeIcons.user),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Top 5 Liked",
                              style:
                                  Theme.of(context).textTheme.bodyText1?.apply(
                                        fontWeightDelta: 1,
                                      ),
                            ),
                            IconButton(
                              onPressed: () async {
                                setState(
                                  () {
                                    _top5Loading = true;
                                  },
                                );
                                bool connected = await _stateTestConnection();
                                if (connected) {
                                  await recipesProvider.reload5Recipes(DBType);
                                }
                                setState(() {
                                  _top5Loading = false;
                                });
                              },
                              icon: Icon(FontAwesomeIcons.repeat),
                            )
                          ],
                        ),
                        _top5Loading
                            ? CircularProgressIndicator()
                            : Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.28,
                                child: PageCarousel(),
                              ),
                      ],
                    ),
                  ),
                  TextButton(
                      style: TextButton.styleFrom(
                          foregroundColor: kYellowColor,
                          backgroundColor: kRosewood,
                          padding: const EdgeInsets.all(13.0),
                          textStyle: const TextStyle(fontSize: 15)),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddRecipePage()),
                        );
                      },
                      child: const Text('Create recipe')),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 4.0),
                    child: Row(
                      children: <Widget>[
                        Switch(
                          activeColor: Theme.of(context).colorScheme.secondary,
                          activeTrackColor:
                              Theme.of(context).colorScheme.primary,
                          inactiveThumbColor: Colors.black,
                          inactiveTrackColor: Colors.black45,
                          value: _switchOnlineState,
                          onChanged: (value) async {
                            setState(() {
                              _switchOnlineState = value;
                              _allLoading = true;
                            });
                            await recipesProvider.getDownloadedRecipes();
                            setState(() {
                              _allLoading = false;
                            });
                          },
                        ),
                        Text(
                          _switchOnlineState
                              ? 'See All Recipes'
                              : 'Downloaded Recipes',
                          style: Theme.of(context).textTheme.bodyText1?.apply(
                                fontWeightDelta: 2,
                              ),
                        ),
                        const Spacer(),
                        if (_switchOnlineState)
                          IconButton(
                            icon: const Icon(
                              FontAwesomeIcons.repeat,
                            ),
                            onPressed: () async {
                              setState(
                                () {
                                  _allLoading = true;
                                },
                              );
                              bool connected = await _stateTestConnection();
                              if (connected) {
                                await recipesProvider.getRecipes(
                                  DBType.firebase,
                                  onDemand: true,
                                  index: _active,
                                );
                              }
                              setState(() {
                                _allLoading = false;
                              });
                            },
                          ),
                      ],
                    ),
                  ),
                  _allLoading
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : !_isConnectedToInternet && _switchOnlineState
                          ? Container(
                              alignment: Alignment.center,
                              width: double.infinity,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text(
                                    'No Internet Connection\nUse Switch to Check Downloaded Recipes',
                                    textAlign: TextAlign.center,
                                  ),
                                  TextButton(
                                    onPressed: () async {
                                      setState(() {
                                        _allLoading = true;
                                      });
                                      await _stateTestConnection();
                                      setState(() {
                                        _allLoading = true;
                                      });
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStatePropertyAll<Color>(
                                                Theme.of(context)
                                                    .primaryColor)),
                                    child: Text(
                                      'REFRESH',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).backgroundColor,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : !_switchOnlineState && downloadedRecipes.isEmpty
                              ? const Center(
                                  child: Text(
                                    'No Downloaded Recipes.\nTry downloading more next time you are online!',
                                  ),
                                )
                              : Expanded(
                                  child: SingleChildScrollView(
                                    physics:
                                        const AlwaysScrollableScrollPhysics(),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10.0),
                                      child: Stack(
                                        alignment: Alignment.bottomCenter,
                                        children: [
                                          Column(
                                            children: [
                                              ...List.generate(
                                                _switchOnlineState
                                                    ? recipes.length
                                                    : downloadedRecipes.length,
                                                (index) {
                                                  return ChangeNotifierProvider
                                                      .value(
                                                    value: _switchOnlineState
                                                        ? recipes[index]
                                                        : downloadedRecipes[
                                                            index],
                                                    child: RecipeListItem(
                                                      downloaded:
                                                          !_switchOnlineState,
                                                    ),
                                                  );
                                                },
                                              ).toList(),
                                            ],
                                          ),
                                          if (_switchOnlineState)
                                            Container(
                                              color: Colors.white,
                                              height: 40,
                                              width: double.infinity,
                                              child: Center(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    ListView.builder(
                                                      shrinkWrap: true,
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      itemCount: min(
                                                          (recipesProvider
                                                                      .totalNumOfRecipes /
                                                                  5)
                                                              .ceil(),
                                                          10),
                                                      itemBuilder:
                                                          ((context, index) {
                                                        return IconButton(
                                                          onPressed: () async {
                                                            setState(() {
                                                              _active = index;
                                                            });
                                                            setState(() {
                                                              _allLoading =
                                                                  true;
                                                            });

                                                            await recipesProvider
                                                                .getRecipes(
                                                                    DBType
                                                                        .firebase,
                                                                    onDemand:
                                                                        true,
                                                                    index:
                                                                        index);

                                                            setState(() {
                                                              _allLoading =
                                                                  false;
                                                            });
                                                          },
                                                          icon: Stack(
                                                            alignment: Alignment
                                                                .center,
                                                            children: [
                                                              Icon(
                                                                Icons
                                                                    .circle_rounded,
                                                                color: index ==
                                                                        _active
                                                                    ? Colors
                                                                        .black
                                                                    : Colors
                                                                        .black26,
                                                                size: index ==
                                                                        _active
                                                                    ? 24
                                                                    : 8,
                                                              ),
                                                              if (index ==
                                                                  _active)
                                                                Text(
                                                                  (index + 1)
                                                                      .toString(),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .center,
                                                                  style:
                                                                      const TextStyle(
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                            ],
                                                          ),
                                                        );
                                                      }),
                                                    ), /*
                                                  Container(
                                                    width: 30,
                                                    height: double.infinity,
                                                    color: Colors.black45,
                                                    alignment: Alignment.center,
                                                    margin:
                                                        const EdgeInsets.all(
                                                            10),
                                                    child: TextFormField(
                                                      onFieldSubmitted:
                                                          (value) {},
                                                      validator: (value) {
                                                        if (value!.isEmpty) {
                                                          return 'Page #';
                                                        } else if ((value
                                                                as int) >=
                                                            (recipesProvider
                                                                        .totalNumOfRecipes /
                                                                    5)
                                                                .ceil()) {
                                                          return 'Up to ${(recipesProvider.totalNumOfRecipes / 5).ceil()}';
                                                        }
                                                        return null;
                                                      },
                                                      keyboardType:
                                                          TextInputType.number,
                                                      inputFormatters: [
                                                        FilteringTextInputFormatter
                                                            .digitsOnly
                                                      ], // Only numbers can be entered
                                                    ),
                                                  ),
                                                  */
                                                  ],
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                ],
              ),
      ),
    );
  }
}
