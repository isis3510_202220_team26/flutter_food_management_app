// ignore_for_file: unnecessary_null_comparison

import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_food_management_app/constants.dart';
import 'package:flutter_food_management_app/controller/pantry_controller.dart';
import 'package:flutter_food_management_app/models/user.dart';
import 'package:flutter_food_management_app/screens/pantry.dart';
import 'package:flutter_food_management_app/screens/shopping_list.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
//import 'package:flutter_food_management_app/model/models/user.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:provider/provider.dart';

import '../controller/user_controller.dart';
//import 'package:image/image.dart' as img;
import 'package:flutter_profile_picture/flutter_profile_picture.dart';

import '../providers/internet_provider.dart';
import 'calories_report.dart';

//import '../models/user.dart';

class UserProfile extends StatefulWidget {
  static const routeName = 'UserProfileScreen';

  //final User user = await _auth.currentUser!();
  //final uid = user.uid;
  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  final UserController _controller = UserController();
  var image;
  final ImagePicker _picker = ImagePicker();

  void onDevelopment(){
    showDialog(
        context: context,
        builder: (context){
          return AlertDialog(
            content: Text("Feature under development, not working yet"),
          );
        }
    );
  }

  Future showCaloriesReport() async {

    InternetProvider internetProvider =
    Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();

    if(internetProvider.connected){
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CaloriesReportPage()),
      );
    }
    else{
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              content: Text("There is no internet connection. Try again later"),
            );
          }
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: Future.wait(
            [_controller.getUserByEmail(), _controller.checkConnectivity()]),
        builder: ((context, AsyncSnapshot<List<dynamic>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('${snapshot.error} occurred',
                    style: TextStyle(fontSize: 18)),
              );
            } else if (snapshot.hasData) {
              Usuario? u = snapshot.data![0];
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: kYellowColor,
                  leading: GestureDetector(
                    onTap: (){
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context){
                          return RecipeOverview();
                        },
                        ),
                      );
                    },
                    child: Icon(
                      Icons.arrow_back,  // add custom icons also
                    ),
                  ),
                  title: Center(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          color: kRufouse,
                        ),
                        Spacer(),
                        Text(
                          u!.email,
                          style: TextStyle(fontSize: 15, color: kRufouse),
                        ),
                        Spacer(),
                        IconButton(
                          icon: Icon(Icons.image),
                          onPressed: (() => _pickImageCamera()),
                          color: kRufouse,
                        ),
                      ],
                    ),
                  ),
                ),
                body: SafeArea(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(40, 70, 20, 40),
                      child: Column(
                        children: [
                          image == (null)
                              ? ProfilePicture(
                                  name: u!.firstName,
                                  radius: 31,
                                  fontsize: 21,
                                )
                              : Image.file(
                                  image,
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 25,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    'FIRST NAME:',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 50,
                                  ),
                                  Text(u!.firstName),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: onDevelopment,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height / 25,
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    'LAST NAME:',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 80,
                                  ),
                                  Text(u.lastName),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: onDevelopment,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height / 25,
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    'EMAIL:',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 50,
                                  ),
                                  Text(u.email),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: onDevelopment,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height / 25,
                              ),
                              Center(
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      'DAILY CALORIES:',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                          50,
                                    ),
                                    Text('${u.dailyCalories}'),
                                    Spacer(),
                                    IconButton(
                                      icon: Icon(Icons.edit),
                                      onPressed: onDevelopment,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Spacer(),
                          snapshot.data![1]
                              ? Text('')
                              : Text(
                                  "No se esta mostrando datos mas recientes. Revisar conectividad",
                                  style: TextStyle(fontSize: 13),
                                  textAlign: TextAlign.center,
                                ),
                          Spacer(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                  style: TextButton.styleFrom(
                                      foregroundColor: kYellowColor,
                                      backgroundColor: kRosewood,
                                      padding: const EdgeInsets.all(13.0),
                                      textStyle: const TextStyle(fontSize: 15)),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PantryScreen()),
                                    );
                                  },
                                  child: const Text('View Pantry')),
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 80,
                              ),
                              TextButton(
                                  style: TextButton.styleFrom(
                                      foregroundColor: kYellowColor,
                                      backgroundColor: Colors.green,
                                      padding: const EdgeInsets.all(10.0),
                                      textStyle: const TextStyle(fontSize: 15)),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ShoppingList()),
                                    );
                                  },
                                  child: Icon(Icons.shopping_cart)),
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 80,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                  style: TextButton.styleFrom(
                                      foregroundColor: kYellowColor,
                                      backgroundColor: kRosewood,
                                      padding: const EdgeInsets.all(13.0),
                                      textStyle: const TextStyle(fontSize: 15)),
                                  onPressed: showCaloriesReport,
                                  child: const Text('Calories Report')),
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 80,
                              ),
                              TextButton(
                                  style: TextButton.styleFrom(
                                      foregroundColor: kYellowColor,
                                      backgroundColor: kRufouse,
                                      padding: const EdgeInsets.all(13.0),
                                      textStyle: const TextStyle(fontSize: 15)),
                                  onPressed: () {},
                                  child: const Text('Delete Account')),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          }
          return Center(child: CircularProgressIndicator());
        }),
      ),
    );
  }

/*
  Future chooseImage(ImageSource source) async {
    final pickedFile = await ImagePicker().pickImage(source: source);
    setState(() {
      imageFile = File(pickedFile!.path);
      _load = false;
    });
  }
*/
  _pickImageCamera() async {
    final XFile? pimage = await _picker.pickImage(source: ImageSource.camera);
    final pickedImageFile = File(pimage!.path);
    setState(() {
      image = pickedImageFile;
    });
  }
}
/*
  _getFromCamera() async {
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }
}
*/
/*
_imgFromCamera() async {
  File pickedImg = await ImagePicker()
      .pickImage(source: ImageSource.camera, imageQuality: 50);
  //SetState(_img = File(pickedImg.path)):
}
*/
