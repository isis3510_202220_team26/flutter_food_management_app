import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/models/recipe.dart';
import 'package:flutter_food_management_app/providers/recipes_list_provider.dart';
import 'package:flutter_food_management_app/providers/user_provider.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:provider/provider.dart';

class RecipeDetails extends StatelessWidget {
  static const routeName = '/recipe-detail';

  const RecipeDetails({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final recipeID = (ModalRoute.of(context)?.settings.arguments as Map)['id']
        as String; // is the id!
    final bool downloaded = (ModalRoute.of(context)?.settings.arguments
        as Map)['downloaded'] as bool;
    final loadedRecipe = Provider.of<RecipesListProvider>(
      context,
      listen: false,
    ).findById(
      recipeID,
      downloaded,
    );
    final auth = Provider.of<UserProvider>(context);
    return SafeArea(
      child: Scaffold(
        body: ChangeNotifierProvider.value(
          value: loadedRecipe,
          child: _RecipeDetailView(downloaded: downloaded),
        ),
      ),
    );
  }
}

class _RecipeDetailView extends StatefulWidget {
  final bool downloaded;
  const _RecipeDetailView({
    Key? key,
    required this.downloaded,
  }) : super(key: key);

  @override
  State<_RecipeDetailView> createState() => _RecipeDetailViewState();
}

class _RecipeDetailViewState extends State<_RecipeDetailView> {
  bool _downloading = false;

  @override
  Widget build(BuildContext context) {
    // is the id!
    final loadedRecipe = Provider.of<Recipe>(context);
    final auth = Provider.of<UserProvider>(context);
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: !widget.downloaded
              ? Image.network(
                  loadedRecipe.imageURL,
                  fit: BoxFit.cover,
                  color: Colors.black38,
                  colorBlendMode: BlendMode.darken,
                )
              : Container(color: Colors.black38),
        ),
        Positioned(
          top: 15,
          right: 15,
          child: IconButton(
            icon: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        Positioned(
          bottom: 15,
          left: 15,
          right: 15,
          child: Container(
            height: MediaQuery.of(context).size.height / 1.35,
            padding: const EdgeInsets.all(25),
            decoration: BoxDecoration(
              color: Colors.black87,
              borderRadius: BorderRadius.circular(25),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        loadedRecipe.title,
                        style: Theme.of(context).textTheme.titleLarge!.apply(
                              color: Colors.white,
                              fontWeightDelta: 200,
                            ),
                      ),
                      const Spacer(),
                      _downloading
                          ? CircularProgressIndicator()
                          : loadedRecipe.downloaded
                              ? Icon(
                                  FontAwesomeIcons.check,
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                )
                              : IconButton(
                                  onPressed: () async {
                                    setState(
                                      () {
                                        _downloading = true;
                                      },
                                    );

                                    await Provider.of<RecipesListProvider>(
                                      context,
                                      listen: false,
                                    ).downloadRecipe(
                                      loadedRecipe.id,
                                      loadedRecipe.title,
                                      loadedRecipe.description,
                                    );
                                    setState(() {
                                      _downloading = false;
                                    });
                                  },
                                  color: Colors.white,
                                  icon: Icon(
                                    //FontAwesomeIcons.check
                                    FontAwesomeIcons.download,
                                  ),
                                ),
                      if (loadedRecipe.imageURL != 'NONE')
                        IconButton(
                          icon: Icon(
                            loadedRecipe.liked
                                ? Icons.favorite
                                : Icons.favorite_border,
                            color: Colors.white,
                          ),
                          onPressed: () =>
                              loadedRecipe.toggleFavoriteStatus(auth.email),
                        )
                    ],
                  ),
                  SizedBox(height: 10),
                  if (loadedRecipe.numberOfLikes > 0)
                    Text(
                      'Liked by: ${loadedRecipe.numberOfLikes} foodies',
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  const SizedBox(height: 15),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: SingleChildScrollView(
                      child: Text(
                        loadedRecipe.description,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 15),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
