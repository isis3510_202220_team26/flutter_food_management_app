import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/controller/houseItem_controller.dart';
import 'package:flutter_food_management_app/controller/pantry_controller.dart';
import 'package:flutter_food_management_app/controller/user_controller.dart';
import 'package:flutter_food_management_app/models/recipe.dart' as re;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/screens/pantry.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_food_management_app/models/user.dart';

import '../models/houseitem.dart';
import '../providers/internet_provider.dart';
import 'package:provider/provider.dart';
import '../models/user.dart';

import '../providers/recipes_list_provider.dart';
import 'package:flutter_food_management_app/constants.dart';

class CaloriesReportPage extends StatefulWidget {


  @override
  State<CaloriesReportPage> createState() => _CaloriesReportPageState();
}

class _CaloriesReportPageState extends State<CaloriesReportPage> {

  var _isConnectedToInternet = false;

  int calories=0;
  double eggCalories=0;
  double milkCalories=0;
  double meatCalories=0;
  double beansCalories=0;
  double cerealCalories=0;
  double breadCalories=0;
  double cheeseCalories=0;
  double chocolateCalories=0;

  double roundDouble(double value, int places){
    num mod = pow(10.0, places);
    return ((value * mod).round().toDouble() / mod);
  }

  Future calculateValues() async {

    InternetProvider internetProvider =
    Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();

    if (internetProvider.connected) {
      setState(() {
        _isConnectedToInternet = true;
      });

      UserController _controller= UserController();
      Usuario u=await _controller.getUserByEmail();

      calories=u.dailyCalories!;
      eggCalories= roundDouble(100*calories/155,2);
      milkCalories= roundDouble(100*calories/42, 2);
      meatCalories= roundDouble(100*calories/143, 2);
      beansCalories= roundDouble(100*calories/347, 2);
      cerealCalories= roundDouble(100*calories/379, 2);
      breadCalories= roundDouble(100*calories/265, 2);
      cheeseCalories= roundDouble(100*calories/402, 2);
      chocolateCalories= roundDouble(100*calories/546, 2);

    }
    else{
      setState(() {
        _isConnectedToInternet = false;
      });
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              content: Text("There is no internet connection. Try again later"),
            );
          }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    calculateValues();
    return FutureBuilder(
        future: Future.wait(
            [calculateValues()]),
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: kYellowColor,
                title: Center(
                  child: Row(
                    children: <Widget>[

                      Text(
                        "Report",
                        style: TextStyle(fontSize: 15, color: kRufouse),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
              backgroundColor: Color(0xff590004),
              body: SafeArea(
                child: SingleChildScrollView(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(height: 30),
                        //Greetings
                        Center(
                          child: Text(
                            "Calories Report",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                              color: Color(0xffF3F3F3),

                            ),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Your daily calories objective: $calories, is equivalent to: ",
                          style: TextStyle(
                            fontSize: 20,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 35),
                        Text(
                          "$eggCalories grams of eggs",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$milkCalories grams of milk",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$meatCalories grams of meat",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$breadCalories grams of bread",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$beansCalories grams of beans",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$cerealCalories grams of cereal",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$cheeseCalories grams of cheese",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "$chocolateCalories grams of chocolate",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xffF3F3F3),
                          ),
                        ),
                        SizedBox(height: 10),

                      ],
                    ),
                  ),
                ),
              ),
            );

  }
    );
  }

}

