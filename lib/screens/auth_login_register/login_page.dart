import 'package:firebase_auth/firebase_auth.dart';
import "package:flutter/material.dart";
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../providers/internet_provider.dart';
import 'forgot_password_page.dart';

class LoginPage extends StatefulWidget{
  final VoidCallback showRegisterPage;
  const LoginPage({Key? key,required this.showRegisterPage}): super(key:key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{

  //Controllers for firebase
  final _emailController=TextEditingController();
  final _passwordController=TextEditingController();

  Future signIn() async {
    InternetProvider internetProvider =
    Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();

    if (internetProvider.connected) {
      try{
        await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _emailController.text.trim(),
          password: _passwordController.text.trim(),
        );
        final prefs= await SharedPreferences.getInstance();
        await prefs.setString("user",_emailController.text.trim() );
      }on FirebaseAuthException catch (e) {
        print(e);
        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                content: Text(e.message.toString()),
              );
            }
        );
      }

    }
    else{
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              content: Text("No internet connection, unable to log in. Try later"),
            );
          }
      );
    }


  }

  @override
  void dispose() {
    // TODO: implement dispose
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF3F3F3),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                //Logo
                Icon(
                    Icons.food_bank_outlined,
                    size:100,
                    color: Color(0xff250001)
                ),
                SizedBox(height: 75),
                //Greetings
                Text(
                  "Welcome again!",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 36,
                    color: Color(0xff590004),
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "Please log-in",
                  style: TextStyle(
                    fontSize: 20,
                    color: Color(0xff590004),
                  ),
                ),
                SizedBox(height: 50),

                //Email
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:20.0),
                      child: TextField(
                        maxLength: 55,
                        controller: _emailController,
                        decoration: InputDecoration(
                          border:InputBorder.none,
                          hintText: "Email",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:20.0),
                      child: TextField(
                        maxLength: 15,
                        controller: _passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border:InputBorder.none,
                          hintText: "Password",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: signIn,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Color(0xffA50104),
                          borderRadius: BorderRadius.circular(12)
                      ),
                      child: Center(
                        child: Text(
                          "Sign In",
                          style: TextStyle(
                            color:Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 25),
                //Forgot password
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context,
                          MaterialPageRoute(builder: (context){
                            return ForgotPasswordPage();
                          },
                          ),
                        );
                      },
                      child: Text("Forgot password?",
                        style: TextStyle(
                          color:Color(0xffFCBA04),
                          fontWeight: FontWeight.bold,
                          fontSize:18,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 25),
                //Not user
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Not a user?",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize:18,
                      ),
                    ),
                    GestureDetector(
                      onTap: widget.showRegisterPage,
                      child: Text(" Register now",
                        style: TextStyle(
                          color:Color(0xffFCBA04),
                          fontWeight: FontWeight.bold,
                          fontSize:18,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 25),
              ],
            ),
          ),
        ),
      ),

    );
  }
}
