import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/screens/auth_login_register/login_page.dart';
import 'package:flutter_food_management_app/screens/auth_login_register/register_page.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({Key? key}) : super(key: key);
  static const routeName = 'AuthPage';

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  bool showLoginPage = true;

  void toggleScreens() {
    setState(() {
      showLoginPage = !showLoginPage;
    });
  }

  Widget buildAuthScreen() {
    if (showLoginPage) {
      return LoginPage(showRegisterPage: toggleScreens);
    } else {
      return RegisterPage(showLoginPage: toggleScreens);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildAuthScreen(),
    );
  }
}
