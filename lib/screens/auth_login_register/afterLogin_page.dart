import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/screens/auth_login_register/auth_page.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
import 'home_page.dart';

class AfterLoginPage extends StatelessWidget {
  const AfterLoginPage({Key? key}) : super(key: key);

  static const routeName = 'AfterLoginPage';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            log(snapshot.data!.email.toString());
            return RecipeOverview();
          } else {
            return AuthPage();
          }
        },
      ),
    );
  }
}
