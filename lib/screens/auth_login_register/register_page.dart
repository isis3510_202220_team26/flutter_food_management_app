import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_food_management_app/models/houseitem.dart';
import 'package:flutter_food_management_app/models/user.dart' as u;
import 'package:flutter_food_management_app/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../providers/internet_provider.dart';

class RegisterPage extends StatefulWidget {
  final VoidCallback showLoginPage;
  const RegisterPage({Key? key, required this.showLoginPage}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  //Controllers for firebase
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _ageController = TextEditingController();
  final _dailyCaloriesController = TextEditingController();
  final _categoriesController = TextEditingController();

  Future register() async {
    InternetProvider internetProvider =
    Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();

    if (internetProvider.connected) {
      try{

        if (_passwordController.text.trim() ==
            _confirmPasswordController.text.trim()) {
          //create user
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: _emailController.text.trim(),
            password: _passwordController.text.trim(),
          );

          //add user details
          addUserDetails(
            _emailController.text.trim(),
            _firstNameController.text.trim(),
            _lastNameController.text.trim(),
            int.parse(_ageController.text.trim()),
            int.parse(_dailyCaloriesController.text.trim()),
            _categoriesController.text.trim(),
          );
          final prefs= await SharedPreferences.getInstance();
          await prefs.setString("user",_emailController.text.trim() );

        }
        else{
          showDialog(
              context: context,
              builder: (context){
                return AlertDialog(
                  content: Text("The passwords are not the same, try again."),
                );
              }
          );
        }

      }on FirebaseAuthException catch (e) {
        print(e);
        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                content: Text(e.message.toString()),
              );
            }
        );
      }

    }
    else{
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              content: Text("No internet connection, unable to register. Try later"),
            );
          }
      );
    }


  }

  Future addUserDetails(String email, String firstName, String lastName,
      int age, int dailyCalories, String categories) async {
    /*
    final user = u.Usuario(
        email: email,
        firstName: firstName,
        lastName: lastName,
        categories: categories,
        age: age,
        dailyCalories: dailyCalories,
        hs: []);
        */
    final user = u.Usuario(
        email: email,
        firstName: firstName,
        lastName: lastName,
        categories: categories,
        age: age,
        dailyCalories: dailyCalories,
        //userName: "",
        hs: [],
        lastPantry: DateTime.now());
    UserProvider().addUser(user);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    _firstNameController.dispose();
    _lastNameController.dispose();
    _ageController.dispose();
    _categoriesController.dispose();
    _dailyCaloriesController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF3F3F3),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(height: 5),
                //Greetings
                Text(
                  "Be part of Foodia",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 36,
                    color: Color(0xff590004),
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  "Please register",
                  style: TextStyle(
                    fontSize: 20,
                    color: Color(0xff590004),
                  ),
                ),
                SizedBox(height: 10),

                //Email
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 30,
                        controller: _emailController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Email",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 15,
                        controller: _passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Password",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Confirm Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 15,
                        controller: _confirmPasswordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Confirm password",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //First name
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 15,
                        controller: _firstNameController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "First name",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //Last name
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 15,
                        controller: _lastNameController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Last name",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //Age
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        maxLength: 3,
                        controller: _ageController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Age",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //Dialy calories
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        maxLength: 5,
                        controller: _dailyCaloriesController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Daily calories objective",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //Categories
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 30,
                        controller: _categoriesController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Food categories you like",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),

                //Button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: register,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Color(0xffA50104),
                          borderRadius: BorderRadius.circular(12)),
                      child: Center(
                        child: Text(
                          "Register",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 25),

                //Not user
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Already a user?",
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    GestureDetector(
                      onTap: widget.showLoginPage,
                      child: Text(
                        " Sign in",
                        style: TextStyle(
                          color: Color(0xffFCBA04),
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 25),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

