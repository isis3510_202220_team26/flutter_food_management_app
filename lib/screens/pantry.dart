import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/constants.dart';
import 'package:flutter_food_management_app/controller/pantry_controller.dart';
import 'package:flutter_food_management_app/screens/user_profile.dart';
import 'package:flutter_food_management_app/test_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_food_management_app/models/user.dart';

import '../controller/user_controller.dart';
import '../models/houseitem.dart';
import 'add_product_page.dart';

//import '../../model/models/houseitem.dart';
//import '../../model/models/user.dart';

class PantryScreen extends StatelessWidget {
  static const routeName = 'PantryScreen';
  final PantryController _controller = PantryController();
  //String username="";
  int difference = 0;

  //TO-DO
  Future calculateDifference() async {
    Usuario u = await UserController().getUserByEmail();
    difference = DateTime.now().difference(u.lastPantry).inDays;
  }

  @override
  Widget build(BuildContext context) {
    calculateDifference();
    return FutureBuilder(
      future: Future.wait(
          [_controller.getHouseItems(), _controller.getMostRecently2Expire(),calculateDifference()]),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                '${snapshot.error} occurred',
                style: TextStyle(fontSize: 18),
              ),
            );
            // if we got our data
          } else if (snapshot.hasData) {
            // Extracting data from snapshot object
            List<HouseItem>? data = snapshot.data![0];
            HouseItem toExpire = snapshot.data![1];
            String expireName = toExpire.name;
            return Scaffold(
              appBar: AppBar(
                title: Center(
                  child: Text(
                    "Pantry",
                    style:
                        TextStyle(color: kRufouse, fontWeight: FontWeight.bold),
                  ),
                ),
                backgroundColor: kYellowColor,
                  leading: GestureDetector(
                    onTap: (){
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context){
                          return UserProfile();
                        },
                        ),
                      );
                    },
                    child: Icon(
                      Icons.arrow_back,  // add custom icons also
                    ),
                  )

              ),

              backgroundColor: kRufouse,
              body: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(12),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 2.5,
                        decoration: BoxDecoration(
                          color: Colors.white24,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Center(
                          child: Table(
                            children: data!.map((e) {
                              return buildRows(e);
                            }).toList(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    //Button
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return AddProductPage();
                              },
                            ),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12)),
                          child: Center(
                            child: Text(
                              "Add product",
                              style: TextStyle(
                                color: Color(0xffA50104),
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 25),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "The last time you bought groceries was $difference day(s) ago",
                          style: TextStyle(
                            color: Color(0xffFCBA04),
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "The $expireName is near to expire. Be careful!",
                      style: TextStyle(
                        color: Color(0xffFCBA04),
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(height: 25),
                    IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).popAndPushNamed('/');
                      },
                    )
                  ],
                ),
              ),
            );
          }
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
    //child: buildScaffold(context));
  }
}


TableRow buildRows(HouseItem e) {

  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(e.expireDate);
  return buildRow([e.name, e.quantity, e.measurement, formatted], false);
}

TableRow buildRow(List<dynamic> cells, bool isHeader) =>
    //TableRow(children: [Text('Hola'), Text('data')]);
    TableRow(
      children: cells.map((e) {
        final styleH = TextStyle(
          fontWeight: isHeader ? FontWeight.bold : FontWeight.normal,
          color: kCultured,
          fontSize: isHeader ? 18 : 15,
        );
        return Padding(
          padding: const EdgeInsets.all(12),
          child: Center(
              child: Text(
            e,
            style: styleH,
          )),
        );
      }).toList(),
    );

Future<String> getData() {
  return Future.delayed(Duration(seconds: 2), () {
    return "I am data";
    // throw Exception("Custom Error");
  });
}
/*
  Future<List<HouseItem>> test() async {
    List<HouseItem> hs = await _controller.getHouseItems();
    f = hs;
    return f;
  }

  
*/