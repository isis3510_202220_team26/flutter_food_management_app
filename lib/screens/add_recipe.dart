import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/controller/houseItem_controller.dart';
import 'package:flutter_food_management_app/controller/pantry_controller.dart';
import 'package:flutter_food_management_app/controller/user_controller.dart';
import 'package:flutter_food_management_app/models/recipe.dart' as re;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/screens/pantry.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_food_management_app/models/user.dart';

import '../models/houseitem.dart';
import '../providers/internet_provider.dart';
import 'package:provider/provider.dart';

import '../providers/recipes_list_provider.dart';

class AddRecipePage extends StatefulWidget {


  @override
  State<AddRecipePage> createState() => _AddRecipePageState();
}

class _AddRecipePageState extends State<AddRecipePage> {
  //Controllers for firebase
  final _descriptionController = TextEditingController();
  final _imageURLController = TextEditingController();
  final _titleController = TextEditingController();

  DateTime _dateTime=DateTime.now();

  var _isConnectedToInternet = false;

  Future addRecipe() async {

    InternetProvider internetProvider =
    Provider.of<InternetProvider>(context, listen: false);
    await internetProvider.testConnection();


    final recipe = re.Recipe(
        id: _titleController.text.trim(),
        title: _titleController.text.trim(),
        description: _descriptionController.text.trim(),
        imageURL: _imageURLController.text.trim(),
        downloaded: false,
        numberOfLikes: 0,
        liked: false,
    );

      final db = FirebaseFirestore.instance;

    RecipesListProvider recipeProvider =
    Provider.of<RecipesListProvider>(context, listen: false);

    recipeProvider.addRecipe(recipe);

      db.collection('recipes').doc(recipe.title).set({
        "dateCreated": _dateTime,
        "description": recipe.description,
        "imageURL": recipe.imageURL,
        "numberOfLikes": recipe.numberOfLikes,
        "title": recipe.title
      });

      Navigator.push(context,
        MaterialPageRoute(builder: (context){
          return RecipeOverview();
        },
        ),
      );//

      if (internetProvider.connected) {
        setState(() {
          _isConnectedToInternet = true;
        });

        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                content: Text("The recipe was added correctly"),
              );
            }
        );
      }
      else{
        setState(() {
          _isConnectedToInternet = false;
        });
        showDialog(
            context: context,
            builder: (context){
              return AlertDialog(
                content: Text("There is no internet connection. Your recipe is pending for addition to the database. As soon as your internet connection is restored the process will resume. Do not try to add it again."),
              );
            }
        );
      }
    }



  @override
  void dispose() {
    _descriptionController.dispose();
    _imageURLController.dispose();
    _titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff590004),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(height: 30),
                //Greetings
                Center(
                  child: Text(
                    "Create a new recipe",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32,
                      color: Color(0xffF3F3F3),

                    ),
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  "Enter information",
                  style: TextStyle(
                    fontSize: 20,
                    color: Color(0xffF3F3F3),
                  ),
                ),
                SizedBox(height: 15),

                //Email
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        maxLength: 30,
                        controller: _titleController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Name of your recipe",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        maxLength: 5000,
                        controller: _descriptionController,
                        obscureText: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(vertical: 30),
                          hintText: "Ingredients and procedure description",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                //Confirm Password
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        maxLength: 200,
                        controller: _imageURLController,
                        obscureText: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Image URL",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),


                SizedBox(height: 10),

                //Button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: addRecipe,
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Colors.yellow,
                          borderRadius: BorderRadius.circular(12)),
                      child: Center(
                        child: Text(
                          "Add recipe",
                          style: TextStyle(
                            color: Color(0xffA50104),
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 25),


              ],
            ),
          ),
        ),
      ),
    );
  }
}

