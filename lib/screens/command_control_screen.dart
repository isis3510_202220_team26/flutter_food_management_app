import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../widgets/custom_card.dart';
import 'auth_login_register/afterLogin_page.dart';

class ItemButton {
  final IconData icon;
  final String title;
  final Color color1;
  final Color color2;
  final String route;

  ItemButton(
    this.icon,
    this.title,
    this.color1,
    this.color2,
    this.route,
  );
}

class CommandControlScreen extends StatelessWidget {
  final items = <ItemButton>[
    ItemButton(
      FontAwesomeIcons.lock,
      'Views by Santiago',
      const Color.fromARGB(255, 18, 17, 17),
      const Color.fromARGB(255, 225, 113, 28),
      AfterLoginPage.routeName,
    ),
    ItemButton(
      FontAwesomeIcons.burger,
      'Views by Angel',
      const Color.fromARGB(255, 225, 113, 28),
      const Color.fromARGB(255, 255, 251, 0),
      RecipeOverview.routeName,
    ),
    ItemButton(
      FontAwesomeIcons.user,
      'Views by Sebastian',
      const Color.fromARGB(255, 18, 17, 17),
      const Color(0xffE65100),
      '',
    ),
  ];

  CommandControlScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[50],
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          ...items
              .map(
                (e) => SlideInLeft(
                  duration: const Duration(milliseconds: 700),
                  child: CustomCard(
                    icon: e.icon,
                    text: e.title,
                    color1: e.color1,
                    color2: e.color2,
                    onPress: () {
                      Navigator.of(context).pushReplacementNamed(e.route);
                    },
                  ),
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}
