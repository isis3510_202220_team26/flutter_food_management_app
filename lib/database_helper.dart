import 'package:flutter_food_management_app/models/shoppinglistItem.dart';
import 'package:flutter_food_management_app/models/user.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'dart:developer';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory dd = await getApplicationDocumentsDirectory();
    String path = join(dd.path, 'local.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
CREATE TABLE users (
    email charater varying NOT NULL,
    firstName character varying NOT NULL,
    lastName character varying NOT NULL,
    age integer,
    dailyCalories integer,
    categories character varying,
    lastPantry character varying
   
)''');
    await db.execute('''CREATE TABLE shoppinglist(
          name character varying NOT NULL,
          quantity character varying NOT NULL,
          measurement character varying NOT NULL,
          description character varying NOT NULL,
          img character varying NOT NULL,
          expireDate character varying NOT NULL)
        ''');
  }

  Future<void> addShoppingListItem(ShoppingListItem shI) async {
    final Database db = await database;
    await db.insert('shoppinglist', toMshI(shI),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> addUser(Usuario u) async {
    final Database db = await database;

    await db.insert('users', toM(u),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<Usuario> getUser() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('users');
    var i = maps.length - 1;
    var temp = fromM(json.encode(maps[i]));
    print("HELPEEEEER");
    print(maps);
    print(maps[0]); //
    return temp;
  }

  Future<List<ShoppingListItem>> getShoppingList() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('shoppinglist');
    List<ShoppingListItem> shI = [];
    for (int i = 0; i < maps.length; i++) {
      //log('Esto es maps[i]');
      //log(json.encode(maps[i]));
      ShoppingListItem t = fromMshI(json.encode(maps[i]));
      //log('Esto es t');
      //log('data: $t');

      shI.add(t);
    }
    //log('Esto es return de getshl');
    //print(shI);
    return shI;
  }

  Map<String, dynamic> toM(Usuario u) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String date = formatter.format(u.lastPantry);
    return {
      'email': u.email,
      'firstName': u.firstName,
      'lastName': u.lastName,
      'dailyCalories': u.dailyCalories,
      'age': u.age,
      'categories': u.categories,
      'lastPantry': date,
    };
  }

  Map<String, dynamic> toMshI(ShoppingListItem shI) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String date = formatter.format(shI.expireDate);
    return {
      'name': shI.name,
      'quantity': shI.quantity,
      'measurement': shI.measurement,
      'description': shI.description,
      'img': shI.img,
      'expireDate': date,
    };
  }

  ShoppingListItem fromMshI(jsonshI) {
    var name = json.decode(jsonshI)['name'];
    var quantity = json.decode(jsonshI)['quantity'];
    var measurement = json.decode(jsonshI)['measurement'];
    var description = json.decode(jsonshI)['description'];
    var img = json.decode(jsonshI)['img'];
    var expireDate = json.decode(jsonshI)['expireDate'];
    expireDate = DateTime.parse(expireDate);
    //print(expireDate);

    return ShoppingListItem(
      name: name,
      quantity: quantity,
      measurement: measurement,
      description: description,
      img: img,
      expireDate: expireDate,
    );
  }

  Usuario fromM(jsonU) {
    var email = json.decode(jsonU)['email'];
    var firstName = json.decode(jsonU)['firstName'];
    var lastName = json.decode(jsonU)['lastName'];
    var dailyCalories = json.decode(jsonU)['dailyCalories'];
    var lastPantry = json.decode(jsonU)['lastPantry'];
    lastPantry = DateTime.parse(lastPantry);
    print(lastPantry);
    //lastPantry = lastPantry.toDate();
    var categories = json.decode(jsonU)['categories'];

    return Usuario(
        email: email,
        firstName: firstName,
        lastName: lastName,
        age: 24,
        dailyCalories: dailyCalories,
        lastPantry: lastPantry,
        categories: categories,
        hs: []);
  }
}
