import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

class InternetProvider with ChangeNotifier {
  bool _connected = false;

  bool get connected => _connected;

  Future<Response> timeoutDefinition() async {
    log(
      'http failed',
    );

    return Response('null', 600);
  }

  Future<void> testConnection() async {
    final response = await get(Uri.http('www.google.com'))
        .timeout(const Duration(seconds: 3), onTimeout: timeoutDefinition)
        .onError(
          ((error, stackTrace) async => await timeoutDefinition()),
        );
    log('http response: $response');
    log('http code: ${response.statusCode}');
    _connected = response.statusCode == 600 ? false : true;
    notifyListeners();
  }
}
