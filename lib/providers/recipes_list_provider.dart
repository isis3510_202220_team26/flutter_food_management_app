import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_food_management_app/daos/recipe_dao.dart';
import 'package:flutter_food_management_app/daos/recipe_dao_firebase.dart';
import 'package:flutter_food_management_app/daos/recipe_dao_sqflite.dart';
import 'package:http/http.dart';

import '../helpers/db_type.dart';
import '../models/recipe.dart';

class RecipesListProvider with ChangeNotifier {
  List<Recipe> _items = [];
  List<Recipe> _top5 = [];
  List<Recipe> _downloaded = [];
  int _totalNumRecipes;
  String authID;

  /// Default DB type is Firebase
  DBType _dbType = DBType.firebase;

  RecipesListProvider(
    this._items,
    this._top5,
    this._totalNumRecipes, {
    required this.authID,
  });

  List<Recipe> get items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._items];
  }

  List<Recipe> get downloadedRecipes {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._downloaded];
  }

  List<Recipe> get top5items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._top5];
  }

  int get totalNumOfRecipes {
    return _totalNumRecipes;
  }

  Future<void> getRecipes(DBType dbType,
      {bool onDemand = false, int index = 1, bool liked = false}) async {
    RecipeDAO recipeDao = RecipeDAOFirebaseImpl();
    _items = [];
    await recipeDao.init(
      authID,
      onDemand: onDemand,
      index: index,
    );
    if (_downloaded.isEmpty) {
      log('download');
      await getDownloadedRecipes();
      log('success downloading');
    }
    _items = recipeDao.getAllRecipes();
    _totalNumRecipes = recipeDao.getTotalNumOfRecipes();
    if (!liked) {
      _downloaded.forEach((downloaded) {
        log(downloaded.id);
        log(downloaded.title);
        int matched = _items.indexWhere(
          (element) {
            if (element.title == downloaded.title) {
              log('FOUND MATCH: ${element.title}');
            }
            return element.title == downloaded.title;
          },
        );
        if (matched != -1) {
          log('FOUND MATCH: $matched');
          Recipe copy = _items[matched];
          copy.changeDownloaded();
          _items[matched] = copy;
        }
      });
    }
    notifyListeners();
  }

  Future<void> getDownloadedRecipes() async {
    RecipeDAO recipeDAO = RecipeDAOSQLiteImpl();
    _downloaded = [];
    await recipeDAO.init('');
    _downloaded = recipeDAO.getAllRecipes();
    notifyListeners();
  }

  Future<void> downloadRecipe(
      String id, String title, String description) async {
    RecipeDAO recipeDAO = RecipeDAOSQLiteImpl();
    _downloaded.add(
      Recipe(id: id, title: title, description: description),
    );
    await recipeDAO.addRecipe(
      Recipe(id: id, title: title, description: description),
    );
    _items.firstWhere((element) => element.id == id).changeDownloaded();
    notifyListeners();
  }

  Future<void> getTop5Recipes(DBType dbType) async {
    RecipeDAO recipeDao = RecipeDAOFirebaseImpl();
    await recipeDao.init(authID);
    _top5 = recipeDao.getTop5FeaturedRecipes();
    notifyListeners();
  }

  Future<void> reload5Recipes(DBType) async {
    RecipeDAO recipeDao = RecipeDAOFirebaseImpl();
    _top5 = [];
    await recipeDao.top5fill(authID);
    _top5 = recipeDao.getTop5FeaturedRecipes();
    notifyListeners();
  }

  Recipe findById(String id, bool downloaded) {
    if (downloaded) {
      return _downloaded.firstWhere((element) => id == element.id);
    }
    return _items.firstWhere((recipe) => id == recipe.id);
  }

  Future<void> addRecipe(
      Recipe recipe) async {
    RecipeDAO recipeDAO = RecipeDAOSQLiteImpl();
    await recipeDAO.addRecipe(
      Recipe(id: recipe.id, title: recipe.title, description: recipe.description),
    );
    _items.firstWhere((element) => element.id == recipe.id).changeDownloaded();
    notifyListeners();
  }

}
