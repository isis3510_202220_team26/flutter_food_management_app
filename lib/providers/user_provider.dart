import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/daos/user_dao.dart';

import '../daos/user_dao_firebase.dart';
import '../helpers/db_type.dart';
import '../models/user.dart';

/// Encapsulates logic required to detect the appropriate
/// UserDAO implementation before calling the UserFactory
///
/// Also handles submission New User Form
class UserProvider extends ChangeNotifier {
  String _email = '';
  String _firstName = '';
  String _lastName = '';
  int _age = 0;
  int _dailyCalories = 0;
  String _categories = '';

  /// Default DB type is Hive
  DBType _dbType = DBType.firebase;

  String get email {
    return _email;
  }

  set email(String email) {
    log('setting email to $email');
    _email = email;
  }

  String get categories {
    return _categories;
  }

  set categories(String categories) {
    log('setting email to $categories');
    _categories = categories;
  }

  String get firstName {
    return _firstName;
  }

  set firstName(String firstName) {
    log('setting firstName to $firstName');
    _firstName = firstName;
  }

  String get lastName {
    return _lastName;
  }

  set lastName(String lastName) {
    log('setting lastName to $lastName');
    _lastName = lastName;
  }

  int get age {
    return _age;
  }

  set age(int age) {
    log('setting age to $age');
    _age = age;
  }

  int get dailyCalories {
    return _dailyCalories;
  }

  set dailyCalories(int dailyCalories) {
    log('setting age to $dailyCalories');
    _dailyCalories = dailyCalories;
  }

  DBType get dbType {
    return _dbType;
  }

  set dbType(DBType dbType) {
    log('setting dbType to $dbType');
    _dbType = dbType;
  }

  Future<String> addUser(Usuario user) async {
    UserDAO userDao = UserDAOFirebaseImpl();
    await userDao.init();
    userDao.addUser(user);
    notifyListeners();
    return 'Added user $user';
  }

  /// Passing the dbType here for demonstration purposes, usually this would be
  /// encapsulated completely within this class itself.
  Future getUsers(DBType dbType) async {
    //TODO-DO <List<User>>
  }

  /// See [addUser] for comments.
  ///
  /// Returns the a String of the result
  Future deleteUser() async {
    //TODO-DO <String>
  }

  /// Clears all users from the currently selected DAO Implementation
  ///
  /// Passing the dbType here for demonstration purposes, usually this would be
  /// encapsulated completely within this class itself. I.e., we would just pick
  /// the value of dbType as is done in [deleteUser] and [addUser]
  Future<void> clearAll(DBType dbType) async {
    //TODO-DO
  }
}
