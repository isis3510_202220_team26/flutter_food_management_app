//import 'package:flutter_food_management_app/model/daos/pantry_dao_imple.dart';
//import 'package:flutter_food_management_app/model/daos/user_dao.dart';
//import 'package:flutter_food_management_app/model/daos/user_dao_imple.dart';
//import 'package:flutter_food_management_app/model/models/houseitem.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_food_management_app/models/shoppinglistItem.dart';
import 'package:isolate/isolate_runner.dart';
import 'package:isolate/load_balancer.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../daos/pantry_dao.dart';
import '../daos/pantry_dao_firebase.dart';
import '../models/houseitem.dart';

//import '../model/daos/pantry_dao.dart';
//import '../model/models/user.dart';

class PantryController extends ControllerMVC {
  LoadBalancer? balancer;
  PantryDao pantryDao = PantryDaoImple();
  //UserDao uDao = UserDaoImple();

  factory PantryController() {
    if (_this == null) _this = PantryController._();
    return _this;
  }
  static PantryController _this = PantryController._();
  PantryController._();

  Future<void> addHI2User(ShoppingListItem shI) async {
    final prefs = await SharedPreferences.getInstance();
    final String? user = prefs.getString("user");

    String? e = user;
    await pantryDao.addHI2User(e!, shI);
  }

  Future<List<HouseItem>> getHouseItems() async {
    //final prefs = await SharedPreferences.getInstance();
    //final String userEmail = prefs.getString("user");
    //print('esto es user emial');
    //print(userEmail);
    //FirebaseAuth _auth = FirebaseAuth.instance;
    //final User? user = _auth.currentUser;
    final prefs = await SharedPreferences.getInstance();
    final String? user = prefs.getString("user");

    String? e = user;
    if (e != null) {
      List<HouseItem> r = await pantryDao.geItems(e);
      return r;
    }
    return [];
    //print('---- En controller imprimiendo r ----');
    //print(r);
  }

  Future<HouseItem> getMostRecently2Expire() async {
    List<HouseItem> his = await getHouseItems();
    balancer ??= await LoadBalancer.create(1, IsolateRunner.spawn);
    return await balancer!.run(auxMethod, his, load: 1);
  }

  HouseItem auxMethod(his) {
    HouseItem ans = HouseItem(
        name: 'name',
        quantity: 'quantity',
        measurement: 'measurement',
        expireDate: DateTime.utc(4000, 1, 1));
    print('Fecha del his 0');
    print(his[0].expireDate);
    print(ans.expireDate);
    for (int i = 0; i < his.length; i++) {
      if (ans.expireDate.isAfter(his[i].expireDate)) {
        print('Entro al if');
        ans = his[i];
      }
    }
    return ans;
  }
/*
  Future<User> getUserByEmail() async {
    Map<String, dynamic> u =
        await uDao.getUserByEmail('sebastianvillamil69@gmail.com');
    User ans = User(
        email: u['email'],
        firstName: u['firstName'],
        lastName: u['lastName'],
        categories: u['categories'],
        age: u['age'],
        dailyCalories: u['dailyCalories']);
    return ans;
  }
  */
}
