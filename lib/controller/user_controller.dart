import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_food_management_app/daos/user_dao.dart';
import 'package:flutter_food_management_app/daos/user_dao_firebase.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'dart:io';

import '../database_helper.dart';
import '../models/user.dart';

class UserController extends ControllerMVC {
  UserDAO uDao = UserDAOFirebaseImpl();

  factory UserController() {
    if (_this == null) _this = UserController._();
    return _this;
  }
  static UserController _this = UserController._();
  UserController._();
  /*
  Future<Usuario> getUserByEmail2() async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    final User? user = _auth.currentUser;
    print('Este es usaurio email');
    //print(user?.email);
    String? e = user?.email;
    print(e);
    //final prefs=await SharedPreferences.getInstance();
    //final String user=prefs.getString("user");
    print(user);
    //await Future.delayed(Duration(seconds: 3));
    Map<String, dynamic> u = await uDao.getUserByEmail(e!);
    print('------- Esto es U ------');
    print(u);
    Usuario ans = Usuario(
        email: u['email'],
        firstName: u['first name'],
        lastName: u['last name'],
        categories: u['categories'],
        age: u['age'],
        dailyCalories: u['dailyCalories'],
        //userName: u['userName'],
        hs: [],
        //CORREGIR CUANDO SE CAMBIE EL USUARIO
        lastPantry: u['last pantry'].toDate());
    /*print(ans.firstName);
    print(ans.lastPantry);
    print(ans.lastName);
    print(ans.dailyCalories);
    print(ans.categories);
    print(ans.email);
    print(ans.age);*/
    //print(ans.userName);
    return ans;
  }
*/
  Future<Usuario> getUserByEmail() async {
    var connectivity = await checkConnectivity();
    Usuario ans;

    //FirebaseAuth _auth = FirebaseAuth.instance;
    //final User? user = _auth.currentUser;
    print('Este es usaurio email');
    //print(user?.email);
    final prefs = await SharedPreferences.getInstance();
    final String? user = prefs.getString("user");

    String? e = user;
    //print(e);
    //await Future.delayed(Duration(seconds: 3));
    if (connectivity && e != null) {
      Map<String, dynamic> u = await uDao.getUserByEmail(e);
      // print('------- Esto es U ------');
      //print(u);
      ans = Usuario(
          email: u['email'],
          firstName: u['first name'],
          lastName: u['last name'],
          categories: u['categories'],
          age: u['age'],
          dailyCalories: u['dailyCalories'],
          lastPantry: u['last pantry'].toDate(),
          hs: []);

      await DatabaseHelper.instance.addUser(ans);
    } else {
      //print('Entro al else del connectivity en el get');
      ans = await DatabaseHelper.instance.getUser();
    }

    return ans;
  }

  Future<bool> checkConnectivity() async {
    bool connectivity = await isConnectedToInternet();

    return connectivity;
  }

  Future<bool> isConnectedToInternet() async {
    return await InternetConnectionChecker().hasConnection;
  }
}
