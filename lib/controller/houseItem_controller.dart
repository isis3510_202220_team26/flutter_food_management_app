import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_food_management_app/daos/user_dao.dart';
import 'package:flutter_food_management_app/daos/user_dao_firebase.dart';
import 'package:flutter_food_management_app/services/houseItem_adapter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';

import '../models/houseitem.dart';

class HouseItemController extends ControllerMVC {


  factory HouseItemController() {
    if (_this == null) _this = HouseItemController._();
    return _this;
  }
  static HouseItemController _this = HouseItemController._();
  HouseItemController._();

  Future<HouseItem> getHouseItemByName(String name) async {
    /*FirebaseAuth _auth = FirebaseAuth.instance;
    final User? user = _auth.currentUser;
    print('Este es usaurio email');
    //print(user?.email);
    String? e = user?.email;
    print(e);*/
    //final prefs=await SharedPreferences.getInstance();
    //final String user=prefs.getString("user");
    //print(user);
    //await Future.delayed(Duration(seconds: 3));
    HouseItemAdapter hiAdapter= HouseItemAdapter();
    Map<String, dynamic> u =
        await hiAdapter.getHouseItemByName(name);
    //print('------- Esto es U ------');
    //print(u);
    HouseItem ans = HouseItem(
        name: u['name'],
        quantity: u['quantity'],
        expireDate: u['expireDate'],
        measurement: u['measurement']);
    /*print(ans.firstName);
    print(ans.lastPantry);
    print(ans.lastName);
    print(ans.dailyCalories);
    print(ans.categories);
    print(ans.email);
    print(ans.age);*/
    //print(ans.userName);
    return ans;
  }
}
