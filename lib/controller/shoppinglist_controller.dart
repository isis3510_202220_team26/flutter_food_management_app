//import 'package:flutter_food_management_app/model/daos/pantry_dao_imple.dart';
//import 'package:flutter_food_management_app/model/daos/user_dao.dart';
//import 'package:flutter_food_management_app/model/daos/user_dao_imple.dart';
//import 'package:flutter_food_management_app/model/models/houseitem.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_food_management_app/daos/shoppinglist_dao.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:isolate/isolate_runner.dart';
import 'package:isolate/load_balancer.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../daos/pantry_dao.dart';
import '../daos/pantry_dao_firebase.dart';
import '../daos/shoppinglist_dao_firebase.dart';
import '../database_helper.dart';
import '../models/houseitem.dart';
import '../models/shoppinglistItem.dart';

//import '../model/daos/pantry_dao.dart';
//import '../model/models/user.dart';

class ShoppingListController extends ControllerMVC {
  //LoadBalancer? balancer;
  ShoppingListDao shoppingListDao = ShoppingListDaoImple();
  //UserDao uDao = UserDaoImple();

  factory ShoppingListController() {
    if (_this == null) _this = ShoppingListController._();
    return _this;
  }
  static ShoppingListController _this = ShoppingListController._();
  ShoppingListController._();

  Future<List<ShoppingListItem>> getShoppingListItems() async {
    var connectivity = await checkConnectivity();
    List<ShoppingListItem> ans = [];

    if (connectivity) {
      ans = await shoppingListDao.geItems();
      for (int i = 0; i < ans.length; i++) {
        await DatabaseHelper.instance.addShoppingListItem(ans[i]);
      }
    } else {
      ans = await DatabaseHelper.instance.getShoppingList();
      ans = ans.sublist(0, 4);
    }

    return ans;
  }

  Future<bool> checkConnectivity() async {
    bool connectivity = await isConnectedToInternet();

    return connectivity;
  }

  Future<bool> isConnectedToInternet() async {
    return await InternetConnectionChecker().hasConnection;
  }
}
