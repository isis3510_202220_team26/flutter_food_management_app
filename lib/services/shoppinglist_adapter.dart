import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/models/shoppinglistItem.dart';

class ShoppingListAdapter {
  FirebaseFirestore db = FirebaseFirestore.instance;

  Future<List<ShoppingListItem>> getShoppingList() async {
    List<ShoppingListItem> shList = <ShoppingListItem>[];
    await db.collection("shoppinglist").get().then((event) {
      for (var doc in event.docs) {
        //print("${doc.data()['name']}");
        ShoppingListItem temp = ShoppingListItem(
            name: doc.data()['name'],
            quantity: doc.data()['quantity'],
            measurement: doc.data()['measurement'],
            expireDate: doc.data()['expireDate'].toDate(),
            description: doc.data()['description'],
            img: doc.data()['img']);
        shList.add(temp);
      }
    });
    return shList;
  }
}
