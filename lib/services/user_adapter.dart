import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/user.dart';

class UserAdapter {
  FirebaseFirestore db = FirebaseFirestore.instance;
  Future<Map<String, dynamic>> getUserByEmail(String email) async {
    Map<String, dynamic> u = {};
    await db.collection("Users").get().then((event) {
      for (var doc in event.docs) {
        if (doc.data()['email'] == email) {
          //BINGO
          u = doc.data();
        }
      }
    });
    return u;
  }
}
