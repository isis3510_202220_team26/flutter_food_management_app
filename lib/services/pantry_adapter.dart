import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/models/shoppinglistItem.dart';

import '../models/houseitem.dart';

class PantryAdapter {
  // Maneja la base de datos de firebase
  //FirebaseFunctions f = FirebaseFunctions.instance;
  FirebaseFirestore db = FirebaseFirestore.instance;
  //Future<List<HouseItem>>

  Future<List<HouseItem>> getHouseItemsByEmail(userEmail) async {
    print('Entro al metodo en pantry adapter');
    List<HouseItem> hIS = <HouseItem>[];
    await db
        .collection('${'Users/' + userEmail}/pantryHouse')
        .get()
        .then((event) {
      for (var doc in event.docs) {
        //print("${doc.data()}");
        HouseItem temp = HouseItem(
            name: doc.data()['name'],
            quantity: doc.data()['quantity'],
            measurement: doc.data()['measurement'],
            expireDate: doc.data()['expireDate'].toDate());
        hIS.add(temp);
      }
    });
    return hIS;
  }

  Future<void> addHI2User(String userEmail, ShoppingListItem shI) async {
    CollectionReference his = FirebaseFirestore.instance
        .collection('${'Users/$userEmail'}/pantryHouse');
    var temp = {
      "expireDate": shI.expireDate,
      "measurement": shI.measurement,
      "name": shI.name,
      "quantity": shI.quantity
    };
    return his
        .doc(shI.name)
        .set(temp)
        .then((value) => print('--- House Item added ----'))
        .catchError((error) => print('----- object: $error'));
  }
/*
  List<HouseItem> r = [];
  // Version Mock sin conexión a Firebase
  /*
  List<HouseItem> getHouseItems() {
    
  
    r.add(HouseItem('loca ome', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Cheese', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('ham', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('eggs', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Bread', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Pasta', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Milk', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Cheese', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('ham', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('eggs', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Bread', '50', 'mg', '10/09/2022'));
    r.add(HouseItem('Pasta', '50', 'mg', '10/09/2022'));
  
    return r;
  }
*/
  HouseItem getHouseItemByName(String name) {
    HouseItem ans = HouseItem('name', 'quantity', 'measurement', 'expireDate');
    for (var i = 0; i <= r.length; i++) {
      if (r[i].name == name) {
        ans = r[i];
      }
    }
    return ans;
  }

  HouseItem addHouseItem(
      String name, String quantity, String measurement, String expireDate) {
    HouseItem hi = HouseItem(name, quantity, measurement, expireDate);
    r.add(hi);
    return hi;
  }

  List<HouseItem> deleteHouseItem(String name) {
    HouseItem dele = HouseItem('name', 'quantity', 'measurement', 'expireDate');
    for (var i = 0; i <= r.length; i++) {
      if (r[i].name == name) {
        r.removeAt(i);
      }
    }
    return r;
  }

  List<HouseItem> updateHouseItem(
      String name, String quantity, String measurement, String expireDate) {
    for (var i = 0; i <= r.length; i++) {
      if (r[i].name == name) {
        r.removeAt(i);
        r.add(HouseItem(name, quantity, measurement, expireDate));
      }
    }
    return r;
  }
  */
}
