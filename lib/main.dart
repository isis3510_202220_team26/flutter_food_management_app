import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_food_management_app/constants.dart';
import 'package:flutter_food_management_app/providers/internet_provider.dart';
import 'package:flutter_food_management_app/providers/recipes_list_provider.dart';
import 'package:flutter_food_management_app/providers/user_provider.dart';
import 'package:flutter_food_management_app/screens/auth_login_register/afterLogin_page.dart';
import 'package:flutter_food_management_app/screens/recipe_details.dart';
import 'package:flutter_food_management_app/screens/recipe_overview.dart';
import 'package:flutter_food_management_app/screens/search_recipe_screen.dart';
import 'package:provider/provider.dart';

import 'firebase_options.dart';
import 'models/user.dart' as model;
import 'screens/auth_login_register/auth_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: InternetProvider()),
        ChangeNotifierProvider.value(value: UserProvider()),
        ChangeNotifierProxyProvider<UserProvider, RecipesListProvider>(
          create: ((context) => RecipesListProvider([], [], 0, authID: '')),
          update: (context, value, previous) => RecipesListProvider(
            previous!.items,
            previous.top5items,
            previous.totalNumOfRecipes,
            authID: value.email,
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: kRufouse,
          backgroundColor: Colors.white,
          colorScheme: const ColorScheme(
            secondary: kYellowColor,
            onError: kRosewood,
            background: Colors.white,
            brightness: Brightness.light,
            error: kRosewood,
            primary: kRufouse,
            onBackground: Colors.white,
            onPrimary: kRufouse,
            onSecondary: kYellowColor,
            onSurface: Colors.white10,
            surface: Colors.white,
          ),
          progressIndicatorTheme: const ProgressIndicatorThemeData(
            circularTrackColor: kRufouse,
            color: kYellowColor,
          ),
          fontFamily: 'QuickSand',
        ),
        debugShowCheckedModeBanner: false,
        routes: {
          RecipeOverview.routeName: (ctx) => const RecipeOverview(),
          AfterLoginPage.routeName: (ctx) => const AfterLoginPage(),
          RecipeDetails.routeName: (context) => const RecipeDetails(),
          SearchRecipeScreen.routeName: (context) => const SearchRecipeScreen(),
        },
        home: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              log(snapshot.data!.email.toString());
              Provider.of<UserProvider>(context, listen: false).email =
                  snapshot.data!.email.toString();
              return RecipeOverview();
            } else {
              return AuthPage();
            }
          },
        ),
      ),
    );
  }
}
