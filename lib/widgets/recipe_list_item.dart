import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:provider/provider.dart';

import '../models/recipe.dart';
import '../screens/recipe_details.dart';

class RecipeListItem extends StatelessWidget {
  final bool downloaded;

  static CacheManager cacheManager = CacheManager(
    Config(
      'recipeListImageCacheKey',
      stalePeriod: const Duration(days: 2),
    ),
  );

  const RecipeListItem({super.key, required this.downloaded});

  @override
  Widget build(BuildContext context) {
    Recipe recipe = Provider.of<Recipe>(context);

    return GestureDetector(
      onTap: () => Navigator.of(context).pushNamed(
        RecipeDetails.routeName,
        arguments: {'id': recipe.id, 'downloaded': downloaded},
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: PhysicalModel(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              color: Colors.black,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20),
              child: !downloaded
                  ? CachedNetworkImage(
                      imageUrl: recipe.imageURL,
                      width: double.infinity,
                      height: 200,
                      cacheManager: cacheManager,
                      fit: BoxFit.fitWidth,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) => Container(
                        color: Theme.of(context).backgroundColor,
                        child: Center(
                          child: CircularProgressIndicator(
                              value: downloadProgress.progress),
                        ),
                      ),
                      errorWidget: (context, url, error) =>
                          Image.asset('assets/images/recipeAssetImage.png'),
                    )
                  : Image.asset('assets/images/recipeAssetImage.png'),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              boxShadow: const [
                BoxShadow(color: Colors.black54, blurRadius: 5.0),
              ],
            ),
            child: Text(
              recipe.title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 17,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
