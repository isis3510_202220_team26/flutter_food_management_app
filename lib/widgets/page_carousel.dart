import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_food_management_app/helpers/db_type.dart';
import 'package:flutter_food_management_app/providers/recipes_list_provider.dart';
import 'package:flutter_food_management_app/screens/recipe_details.dart';
import 'package:provider/provider.dart';

import '../models/recipe.dart';

class PageCarousel extends StatefulWidget {
  @override
  _PageCarouselState createState() => _PageCarouselState();
}

class _PageCarouselState extends State<PageCarousel> {
  final PageController _controller = PageController();
  int _active = 0;
  static CacheManager cacheManager = CacheManager(
    Config(
      'recipeImageCacheKey',
      stalePeriod: const Duration(days: 1),
    ),
  );

  @override
  void didChangeDependencies() async {
    await Provider.of<RecipesListProvider>(context, listen: false)
        .getTop5Recipes(DBType.firebase);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    RecipesListProvider provider = Provider.of<RecipesListProvider>(context);
    final recipes = provider.top5items;

    return Column(
      children: [
        Expanded(
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                child: PageView.builder(
                  controller: _controller,
                  itemCount: recipes.length,
                  pageSnapping: true,
                  onPageChanged: (value) {
                    setState(() {
                      _active = value;
                    });
                  },
                  itemBuilder: (ctx, id) {
                    return ChangeNotifierProvider.value(
                      value: recipes[id],
                      child: Consumer<Recipe>(
                        builder: (ctx, recipe, child) => GestureDetector(
                          onTap: () {
/*                              Navigator.pushNamed(
                              context,
                              RecipeDetails.routeName,
                              arguments: {'id': recipe.id, 'downloaded': false},
                            );*/
                          },
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 22,
                                  vertical: 8,
                                ),
                                child: PhysicalModel(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  color: Colors.black,
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(20),
                                  child: CachedNetworkImage(
                                    imageUrl: recipe.imageURL,
                                    width: double.infinity,
                                    height: 200,
                                    cacheManager: cacheManager,
                                    fit: BoxFit.fitWidth,
                                    progressIndicatorBuilder:
                                        (context, url, downloadProgress) =>
                                            Container(
                                      color: Theme.of(context).backgroundColor,
                                      child: Center(
                                        child: CircularProgressIndicator(
                                            value: downloadProgress.progress),
                                      ),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Image.asset(
                                            'assets/images/recipeAssetImage.png'),
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: Colors.black54,
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    child: Text(
                                      '#${id + 1}: ${recipe.title}',
                                      style: const TextStyle(
                                        fontSize: 21,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 8.0,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: _active > 0
                    ? Container(
                        decoration: const BoxDecoration(
                            color: Colors.black38, shape: BoxShape.circle),
                        child: IconButton(
                          icon: const Icon(
                            Icons.chevron_left,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            _controller.previousPage(
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.easeIn,
                            );
                          },
                        ),
                      )
                    : null,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: _active < recipes.length - 1
                    ? Container(
                        decoration: const BoxDecoration(
                            color: Colors.black38, shape: BoxShape.circle),
                        child: IconButton(
                          icon: const Icon(
                            Icons.chevron_right,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            _controller.nextPage(
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.decelerate,
                            );
                          },
                        ),
                      )
                    : null,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 40,
          width: double.infinity,
          child: Center(
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: recipes.length,
              itemBuilder: ((context, index) {
                return IconButton(
                  onPressed: () {
                    setState(() {
                      _active = index;
                    });
                    _controller.animateToPage(
                      index,
                      duration: const Duration(milliseconds: 600),
                      curve: Curves.decelerate,
                    );
                  },
                  icon: Stack(
                    alignment: Alignment.center,
                    children: [
                      Icon(
                        Icons.circle_rounded,
                        color: index == _active ? Colors.black : Colors.black26,
                        size: index == _active ? 24 : 8,
                      ),
                      if (index == _active)
                        Text(
                          (index + 1).toString(),
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                    ],
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
