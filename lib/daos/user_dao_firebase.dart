import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/helpers/db_type.dart';

import '../models/user.dart';
import '../services/user_adapter.dart';
import 'user_dao.dart';

class UserDAOFirebaseImpl implements UserDAO {
  /// Note this Box type is as defined in the hive_flutter package.
  late final List<Usuario> users;
  bool _isUserInit = false;
  late final FirebaseFirestore db;
  final UserAdapter uAdapter = UserAdapter();

  /// Singleton implementation
  static final UserDAOFirebaseImpl _instance =
      UserDAOFirebaseImpl._privateConstructor();

  /// Empty private constructor, as we are initialising the async init() in a
  /// separate call.
  UserDAOFirebaseImpl._privateConstructor();

  factory UserDAOFirebaseImpl() {
    return _instance;
  }

  /// Should only be initialised once the first time the Singleton is accessed,
  /// from then on the cached DAOImpl instance is returned.
  @override
  Future<void> init() async {
    if (!_isUserInit) {
      log('initializing UserDAOImpl singleton');
      db = FirebaseFirestore.instance;
      _isUserInit = true;
      log('initialized UserDAORecipeImpl singleton');
    }
  }

  @override
  void addUser(Usuario user) async {
    db.collection("Users").doc(user.email).set({
      "email": user.email,
      "first name": user.firstName,
      "last name": user.lastName,
      "age": user.age,
      "dailyCalories": user.dailyCalories,
      "categories": user.categories,
      "last pantry": user.lastPantry
    });
    db.collection('${'Users/' + user.email}/pantryHouse').doc("initialProduct").set({
      "name": "Product name",
      "quantity": "quantity",//ARREGLAR
      "measurement": "measurement",
      "expireDate": DateTime(2100),
    });
  }

  @override
  Future<void> clear() {
    // TODO: implement clear
    throw UnimplementedError();
  }

  @override
  Usuario? deleteUser(Usuario user) {
    // TODO: implement
    throw UnimplementedError();
  }

  @override
  List<Usuario> getAllUsers() {
    // TODO: implement
    throw UnimplementedError();
  }

  @override
  DBType getImplType() {
    // TODO: implement getImplType
    throw UnimplementedError();
  }

  @override
  String getTitle() {
    // TODO: implement getTitle
    throw UnimplementedError();
  }

  @override
  Future<Map<String, dynamic>> getUserByEmail(String email) async {
    return await uAdapter.getUserByEmail(email);
  }
}
