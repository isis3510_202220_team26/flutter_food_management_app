import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_food_management_app/helpers/db_type.dart';

import '../models/recipe.dart';
import 'recipe_dao.dart';

class RecipeDAOFirebaseImpl implements RecipeDAO {
  /// Note this Box type is as defined in the hive_flutter package.
  List<Recipe> recipes = [];
  List<Recipe> top5 = [];
  int recipeTotalNum = 0;
  bool _isRecipeInit = false;
  FirebaseFirestore? db;

  /// Singleton implementation
  static final RecipeDAOFirebaseImpl _instance =
      RecipeDAOFirebaseImpl._privateConstructor();

  /// Empty private constructor, as we are initialising the async init() in a
  /// separate call.
  RecipeDAOFirebaseImpl._privateConstructor();

  factory RecipeDAOFirebaseImpl() {
    return _instance;
  }

  /// Should only be initialised once the first time the Singleton is accessed,
  /// from then on the cached DAOImpl instance is returned.
  @override
  Future<void> init(
    String authID, {
    bool onDemand = false,
    int index = 0,
  }) async {
    if (db == null) {
      log('initializing RecipeDAOImpl singleton');
      db = FirebaseFirestore.instance;
      db!.settings = const Settings(
        persistenceEnabled: true,
      );
    }
    if (!_isRecipeInit || onDemand) {
      int sizePages = 5;
      int startLimit = ((index) * sizePages);
      log(
        'limit to start: ${startLimit.toString()}',
      );
      int endLimit = startLimit + sizePages;

      recipeTotalNum = (await db!.collection("recipes").get()).docs.length;

      final first = db!
          .collection("recipes")
          .orderBy("dateCreated", descending: false)
          .limit(endLimit);

      first.get().then(
        (documentSnapshot) async {
          final lastVisible = documentSnapshot.docs[startLimit + 1].data();
          log(lastVisible.toString());
          final ref = db!
              .collection("recipes")
              .where('dateCreated',
                  isLessThanOrEqualTo: lastVisible['dateCreated'])
              .orderBy('dateCreated', descending: true)
              .limit(sizePages)
              .withConverter(
                fromFirestore: Recipe.fromFirestore,
                toFirestore: (Recipe recipe, _) => recipe.toFirestore(),
              );
          final docSnap = await ref
              .get(const GetOptions(source: Source.server))
              .onError((error, stackTrace) {
            log(error.toString(), error: error);
            throw UnimplementedError();
          });
          recipes = [];
          log('res size: ${docSnap.docs.length}');
          docSnap.docs.forEach((element) {
            Recipe newRecipe = element.data();
            final recipeID = newRecipe.id;
            log('user authid: $authID');
            final relRefLike = db!.collection("user_recipe_likes").doc(authID);

            relRefLike.get().then(
              (res) {
                log("Successfully completed: ${res.data()}");
                log("Extraction ${(res.data())?[newRecipe.title]}");
                newRecipe.liked = (res.data())?[newRecipe.title] == null
                    ? false
                    : ((res.data())?[newRecipe.title] as bool);
              },
              onError: (e) => log("Error completing: $e"),
            );
            recipes.add(newRecipe);
          }); // Convert to Recipe object
        },
      );

      _isRecipeInit = true;
      log('initialized RecipeDAORecipeImpl singleton');
    }
  }

  @override
  Future<void> addRecipe(Recipe recipe) {
    // TODO: implement addrecipe
    throw UnimplementedError();
  }

  @override
  List<Recipe> getAllRecipes() {
    return recipes;
  }

  @override
  DBType getImplType() {
    // TODO: implement getImplType
    throw UnimplementedError();
  }

  @override
  List<Recipe> getRecipesByName(String name) {
    // TODO: implement getrecipesByName
    throw UnimplementedError();
  }

  @override
  List<Recipe> getTop5FeaturedRecipes() {
    return top5;
  }

  @override
  Future<void> top5fill(String authID) async {
    final ref = db!.collection("recipes").withConverter(
          fromFirestore: Recipe.fromFirestore,
          toFirestore: (Recipe recipe, _) => recipe.toFirestore(),
        );
    final docSnap = await ref
        .orderBy('numberOfLikes', descending: true)
        .limit(5)
        .get(
          const GetOptions(source: Source.serverAndCache),
        )
        .onError(
      (error, stackTrace) {
        top5 = [];
        throw Exception('Could not Retrieve Top 5 from Cache or Server');
      },
    );
    top5 = [];
    docSnap.docs.forEach((element) {
      Recipe newRecipe = element.data();
      final recipeID = newRecipe.id;
      final relRefLike = db!.collection("user_recipe_likes").doc(authID);

      relRefLike.get().then(
        (res) {
          log("Successfully completed: ${res.data()}");
          newRecipe.liked = (res.data())?[newRecipe.title] ?? false;
        },
        onError: (e) => log("Error completing: $e"),
      );
      top5.add(newRecipe);
    });
  }

  @override
  Future<void> updateFavoriteStatusDB(
      String authID, String recipeID, int newLikes, bool newStatus) async {
    final relRefLike = db!.collection("user_recipe_likes").doc(authID);

    relRefLike.set({recipeID: newStatus}, SetOptions(merge: true)).then(
        (value) {
      log("Successfully completed update");
      db!
          .collection("recipes")
          .doc(recipeID)
          .set({'numberOfLikes': newLikes}, SetOptions(merge: true))
          .then(
            (value) => log('Successfully updated recipe num of likes'),
          )
          .onError(
            (error, stackTrace) => log('error updating recipe: $error'),
          );
    }, onError: (e) {
      log('error updating: $e');
    });
  }

  @override
  String getTitle() {
    // TODO: implement getTitle
    throw UnimplementedError();
  }

  @override
  int getTotalNumOfRecipes() {
    return recipeTotalNum;
  }
}
