import '../models/houseitem.dart';
import '../models/pantry.dart';
import '../models/shoppinglistItem.dart';

abstract class ShoppingListDao {
  Future<void> init();

  Future<List<ShoppingListItem>> geItems();

/*
  void getItemByName(String name);

  void addItem(
      String name, String quantity, String measurement, String expireDate);

  void updateListItem(
      String name, String quantity, String measurement, String expireDate);
      */
}
