import 'dart:ffi';

import 'package:flutter_food_management_app/models/shoppinglistItem.dart';

import '../models/houseitem.dart';
import '../services/pantry_adapter.dart';
import '../daos/pantry_dao.dart';

class PantryDaoImple implements PantryDao {
  //late List<HouseItem> houseitems = <HouseItem>[];
  final PantryAdapter pantryAdapter = PantryAdapter();

  @override
  Future<void> init() async {}

  @override
  Future<List<HouseItem>> geItems(userEmail) async {
    List<HouseItem> houseItems =
        await pantryAdapter.getHouseItemsByEmail(userEmail);
    return houseItems;
  }

  @override
  Future<void> addHI2User(String userEmail, ShoppingListItem shI) async {
    await pantryAdapter.addHI2User(userEmail, shI);
  }

/*
  @override
  HouseItem getItemByName(String name) {
    HouseItem r = pantryAdapter.getHouseItemByName(name);
    return r;
  }

  @override
  void addItem(
      String name, String quantity, String measurement, String expireDate) {
    pantryAdapter.addHouseItem(name, quantity, measurement, expireDate);
  }

  @override
  void updateListItem(
      String name, String quantity, String measurement, String expireDate) {
    // TODO: implement updateListItem
    pantryAdapter.updateHouseItem(name, quantity, measurement, expireDate);
  }
  */
}
