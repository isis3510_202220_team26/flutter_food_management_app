import '../models/houseitem.dart';
import '../models/pantry.dart';
import '../models/shoppinglistItem.dart';

abstract class PantryDao {
  Future<void> init();

  Future<List<HouseItem>> geItems(String userEmail);
  Future<void> addHI2User(String userEmail, ShoppingListItem shI);

/*
  void getItemByName(String name);

  void addItem(
      String name, String quantity, String measurement, String expireDate);

  void updateListItem(
      String name, String quantity, String measurement, String expireDate);
      */
}
