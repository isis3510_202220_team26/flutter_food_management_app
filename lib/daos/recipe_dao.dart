import 'package:flutter_food_management_app/models/recipe.dart';

import '../helpers/db_type.dart';

abstract class RecipeDAO {
  /// Initialise dao
  Future<void> init(String authID, {bool onDemand = false, int index = 1});

  /// Initialise dao
  Future<void> top5fill(String authID);

  /// Returns the name of the Implementation
  /// Only really used for demonstration purposes here
  String getTitle();

  /// Returns the type of the Implementation
  /// Only really used for demonstration purposes here
  DBType getImplType();

  /// Returns a list of all persisted users
  List<Recipe> getAllRecipes();

  /// Returns a list of all persisted users
  List<Recipe> getTop5FeaturedRecipes();

  /// Returns a list of recipes with the given name
  List<Recipe> getRecipesByName(String name);

  /// Persists a recipe
  Future<void> addRecipe(Recipe recipe);

  int getTotalNumOfRecipes();

  Future<void> updateFavoriteStatusDB(
      String authID, String recipeID, int newLikes, bool newStatus);
}
