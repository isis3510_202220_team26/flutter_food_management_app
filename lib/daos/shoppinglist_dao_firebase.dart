import 'package:flutter_food_management_app/daos/shoppinglist_dao.dart';
import 'package:flutter_food_management_app/services/shoppinglist_adapter.dart';

import '../models/houseitem.dart';
import '../models/shoppinglistItem.dart';
import '../services/pantry_adapter.dart';
import '../daos/pantry_dao.dart';

class ShoppingListDaoImple implements ShoppingListDao {
  //late List<HouseItem> houseitems = <HouseItem>[];
  final ShoppingListAdapter shoppinglistAdapeter = ShoppingListAdapter();

  @override
  Future<void> init() async {}

  @override
  Future<List<ShoppingListItem>> geItems() async {
    List<ShoppingListItem> shList =
        await shoppinglistAdapeter.getShoppingList();
    return shList;
  }
}
