import 'package:flutter_food_management_app/models/user.dart';

import '../helpers/db_type.dart';

abstract class UserDAO {
  /// Initialise dao
  Future<void> init();

  /// Returns the name of the Implementation
  /// Only really used for demonstration purposes here
  String getTitle();

  /// Returns the type of the Implementation
  /// Only really used for demonstration purposes here
  DBType getImplType();

  /// Returns a list of all persisted users
  List<Usuario> getAllUsers();

  /// Returns user with a given email
  Future<Map<String, dynamic>> getUserByEmail(String email);

  /// Persists a recipe
  void addUser(Usuario user);

  /// Returns the deleted recipe object if delete was successful, otherwise
  /// will return null if recipe was not found.
  Usuario? deleteUser(Usuario user);

  /// Clears/Deletes all persisted users
  ///
  /// Although 2 out of 3 implementations in this example do not involve any
  /// asynchrony, we need to return a Future due to Hive's clear implementation
  /// requiring an await. Small price to pay for abstraction.
  Future<void> clear();
}
