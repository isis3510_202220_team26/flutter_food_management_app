import 'dart:developer';

import 'package:flutter_food_management_app/daos/recipe_dao.dart';
import 'package:flutter_food_management_app/models/recipe.dart';
import 'package:flutter_food_management_app/helpers/db_type.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;

class RecipeDAOSQLiteImpl implements RecipeDAO {
  final table = 'recipes';
  List<Recipe> recipes = [];
  Database? db;

  @override
  Future<void> init(String authID,
      {bool onDemand = false, int index = 1}) async {
    final dbPath = await getDatabasesPath();
    try {
      db = await openDatabase(path.join(dbPath, 'recipes.db'),
          onCreate: (db, version) async {
        return await db.execute(
            'CREATE TABLE IF NOT EXISTS recipes(id TEXT PRIMARY KEY, title TEXT, description TEXT)');
      }, version: 2);
      await db!.execute(
          'CREATE TABLE IF NOT EXISTS recipes(id TEXT PRIMARY KEY, title TEXT, description TEXT)');
    } catch (dbException) {
      log(dbException.toString());
    }
    List<Map<String, dynamic>> mappedData = await db!.query(table);
    recipes = mappedData.map((element) => Recipe.fromSQLite(element)).toList();
  }

  @override
  Future<void> addRecipe(Recipe recipe) async {
    if (db == null) {
      await init('');
    }
    db!.insert(
      table,
      recipe.toSQLite(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  @override
  List<Recipe> getAllRecipes() {
    return recipes;
  }

  @override
  DBType getImplType() {
    // TODO: implement getImplType
    throw UnimplementedError();
  }

  @override
  List<Recipe> getRecipesByName(String name) {
    // TODO: implement getRecipesByName
    throw UnimplementedError();
  }

  @override
  String getTitle() {
    // TODO: implement getTitle
    throw UnimplementedError();
  }

  @override
  List<Recipe> getTop5FeaturedRecipes() {
    // TODO: implement getTop5FeaturedRecipes
    throw UnimplementedError();
  }

  @override
  Future<void> top5fill(String authID) {
    // TODO: implement top5fill
    throw UnimplementedError();
  }

  @override
  Future<void> updateFavoriteStatusDB(
      String authID, String recipeID, int newLikes, bool newStatus) {
    // TODO: implement updateFavoriteStatusDB
    throw UnimplementedError();
  }

  @override
  int getTotalNumOfRecipes() {
    // TODO: implement getTotalNumOfRecipes
    throw UnimplementedError();
  }
}
