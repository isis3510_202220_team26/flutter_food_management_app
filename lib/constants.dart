import 'package:flutter/material.dart';

// Color Scheme
const kYellowColor = Color(0XFFFCBA04);
const kRufouse = Color(0XFFA50104);
const kRosewood = Color(0XFF590004);
const kDarkSienna = Color(0XFF250001);
const kCultured = Color(0XFFF3F3F3);
